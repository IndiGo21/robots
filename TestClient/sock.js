'use strict';
let sock;

function initSock(name) {
    sock = new WebSocket('ws://localhost:59286');
    sock.onopen = e => {
        sock.send(name);
        document.getElementById('nameInputWrapper')
            .style['display'] = 'none';
        shouldDraw = true;
    };
    sock.onmessage = e => {
        let msg = JSON.parse(e.data);
        console.log('Received message: ', msg);
        if (updateMsgHandler[msg[0]]) {
            updateMsgHandler[msg[0]](msg);
        }
    };
}