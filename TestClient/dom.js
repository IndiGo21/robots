'use strict';

function onPlay(event){
    event.preventDefault();
    let name = document.getElementById('nameInput').value;
    if (name.trim().length != 0 && /^[\w _-]+$/.test(name)) {
        initSock(name);
    }
    else{
        document.getElementById('nameInput')
        .style['border-color'] = 'red';

    }
}

document.getElementById('nameInput').addEventListener('input', function(event){
    let name = document.getElementById('nameInput').value;
    if (name.trim().length != 0 && /^[\w _-]+$/.test(name)){
        document.getElementById('nameInput')
        .style['border-color'] = 'cyan';
    }
    else if (name.trim().length != 0){
        document.getElementById('nameInput')
        .style['border-color'] = 'red';
    }
    else{
        document.getElementById('nameInput')
        .style['border-color'] = 'azure';
    }

})

document.getElementById('nameInputWrapper').addEventListener(
    'submit',onPlay
);

document.querySelector('.buttonPlay').addEventListener(
    'click',onPlay
);


let betInput = {
    _text: '',
    set text(v) {
        if (v.length > 2) { return; }
        if (v == 0) { v = ''; }
        this._text = v;
        document.getElementById('betInput').innerText = this._text;
        if (this._text.length == 0) {
            this._hide();
        } else {
            this._show();
        }
    },
    get text() {
        return this._text;
    },
    _show: function () {
        this._elem.style.display = 'flex';
    },
    _hide: function () {
        this._elem.style.display = 'none';
    },
    isHidden: function () {
        return this._elem.style.display == 'none';
    },
    _elem: document.getElementById('betInputWrapper')
};

document.addEventListener('keypress', function (event) {
    if (!shouldDraw) { return; }
    event.preventDefault();
    if (event.key >= 0 && event.key <= 9 && document.getElementById('nameInputWrapper').style['display'] == 'none') {
        betInput.text += event.key;
    } else if (event.code == "KeyR") {
        sock.send(JSON.stringify(["reset"]));
    } else if (event.code == "KeyE") {
        sock.send(JSON.stringify(["undo"]));
    } else if (event.key == "Enter" && !betInput.isHidden()) {
        sock.send(JSON.stringify(['bet', Number(betInput.text)]));
        betInput.text = '';
    }
});

document.addEventListener('keydown', function (event) {
    if (!shouldDraw) { return; }
    if (event.code == "Backspace") {
        event.preventDefault();
        if (!betInput.isHidden()) {
            betInput.text = betInput.text.substr(0, betInput.text.length - 1);
        }
    } else if (event.code == "Escape" && !betInput.isHidden()) {
        event.preventDefault();
        betInput.text = '';
    }
});

// document.getElementById('betInputWrapper').addEventListener('submit', function (event) {
//     event.preventDefault();
//     let text = document.getElementById('betInput').value;
//     let bet = Number.parseInt(text);
//     if (bet === bet) {
//         sock.send(JSON.stringify(['bet', bet]));
//         document.getElementById('betInput').value = '';
//     }
//     document.getElementById('betInput').blur();
//     document.getElementById('betInputWrapper').style.display = 'none';
// });

function redrawScoreboard() {
    let scoreboard = document.getElementById('top_container');
    scoreboard.innerHTML = '';
    for (let id in players) {
        let player = players[id];
        let element = document.createElement('div');
        element.className = 'scoreboardItem';
        if (id == selectedPlayerId) {
            element.classList.add("selected");
        }
        let bethtml = players[id].bet ? `<div class="bet">${players[id].bet}</div> ` : '<div>-</div>';
        // element.style.color = player.color;
        element.innerHTML = `<div class="playerName">${player.name}</div>` + bethtml + `<div class="playerName">${player.score}</div>`;
        scoreboard.appendChild(element);
    };
}