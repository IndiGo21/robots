'use strict';

function onMouseDragged() {
}

function onMousePressed() {
    stroke('red');
    const hd = 1.5;
    line(mouseX - hd, mouseY - hd, mouseX + hd, mouseY + hd);
    line(mouseX + hd, mouseY - hd, mouseX - hd, mouseY + hd);

    selectRobot();
}

function mouseReleased() {
    if (selectedRobotC) {

        if (dstToRobot(selectedRobotC) > sizes.cellSide * 1) {
            sock.send(JSON.stringify(["move", selectedRobotC, ...dirFromRobot(selectedRobotC)]));
        }
    }
    selectedRobotC = null;
}



let dst = (x1, y1, x2, y2) => Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);

function selectRobot() {
    let old = selectedRobotC;
    let closest = getClosestRobot(mouseX, mouseY);
    if (dstToRobot(closest) <= sizes.cellSide * 1) {
        selectedRobotC = closest;
    }
}

function dstToRobot(color) {
    let rpos = Cell.fromTable(game.robots[color].x, game.robots[color].y).center;
    return dst(mouseX, mouseY, rpos.x, rpos.y);
}

function getClosestRobot(x, y) {
    let clickInTable = new Cell(mouseX, mouseY).table;

    let minc = null;
    let mind = Infinity;
    for (let c in game.robots) {
        // if (game.robots[c].x != clickInTable.x &&
        //     game.robots[c].y != clickInTable.y) { continue; }
        const rx = game.robots[c].x * sizes.cellSide + sizes.cellSide / 2;
        const ry = game.robots[c].y * sizes.cellSide + sizes.cellSide / 2;
        const cd = dst(rx, ry, mouseX, mouseY);
        if (cd < mind) {
            mind = cd;
            minc = c;
        }
    }
    return minc;
}

function dirFromRobot(color) {
    let dx = mouseX - (game.robots[color].x * sizes.cellSide + sizes.cellSide / 2);
    let dy = mouseY - (game.robots[color].y * sizes.cellSide + sizes.cellSide / 2);
    let vec = [];
    if (Math.abs(dx) > Math.abs(dy)) {
        vec = [Math.sign(dx), 0];
    } else {
        vec = [0, Math.sign(dy)];
    }
    return vec;
}