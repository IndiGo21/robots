'use strict';

function drawGrid() {
    stroke(50);
    for (let i = 0; i <= sizes.w; i += sizes.cellSide) {
        line(i, 0, i, sizes.h);
    }
    for (let j = 0; j <= sizes.h; j += sizes.cellSide) {
        line(0, j, sizes.w, j);
    }
    fill(0);
    rect(sizes.cellSide * (sizes.boardW / 2 - 1), sizes.cellSide * (sizes.boardH / 2 - 1), 2 * sizes.cellSide, 2 * sizes.cellSide);
    noFill();
}

function drawTimer() {
    if (!game.timer.startTime) { return; }
    const clamp = (x, a, b) => Math.min(Math.max(x, a), b);
    let t = 1 - (millis() - game.timer.startTime) / 1000 / game.timer.waitTime;
    let t1 = clamp(t * 4, 0, 1) - 0;
    let t2 = clamp(t * 4, 1, 2) - 1;
    let t3 = clamp(t * 4, 2, 3) - 2;
    let t4 = clamp(t * 4, 3, 4) - 3;
    if(canMove()){
        stroke('red');}
    else {
        stroke('cyan');
    }
    strokeWeight(sizes.strokeW_bold);
    const d = sizes.cellSide * 7;
    if (t1) { line(d, d, d + (sizes.w - 2 * d) * t1, d); }
    if (t2) { line(sizes.w - d, d, sizes.w - d, d + (sizes.h - 2 * d) * t2); }
    if (t3) { line(sizes.w - d, sizes.h - d, d + (sizes.w - 2 * d) * (1 - t3), sizes.h - d); }
    if (t4) { line(d, sizes.h - d, d, d + (sizes.h - 2 * d) * (1 - t4)); }
    strokeWeight(sizes.strokeW);
}

function drawWalls() {
    for (let i in game.board.data) {
        let cell = getCellByIndex(i);
        for (let dir in game.board.data[i]) {
            if (game.board.data[i][dir] === true) {
                onDrawWall[dir](cell);
            }
        }
    }
}

function lerpRobots() {
    const lerp = (a, b, t) => a + (b - a) * t;
    const speed = 0.5;
    for (let c in lerpedRobots) {
        lerpedRobots[c].x = lerp(lerpedRobots[c].x, game.robots[c].x, speed);
        lerpedRobots[c].y = lerp(lerpedRobots[c].y, game.robots[c].y, speed);
    }
}

function drawRobots() {
    for (let c in lerpedRobots) {
        let cell = Cell.fromTable(lerpedRobots[c].x, lerpedRobots[c].y);
        if (c == selectedRobotC) {
            onDrawSelectedRobot(cell, c);
        } else {
            onDrawRobot(cell, c);
        }
    }
    strokeWeight(sizes.strokeW);
}

function drawTarget() {
    onDrawTarget(
        Cell.fromTable(game.target.pos.x, game.target.pos.y)
    );
}

const getCellByIndex = i => Cell.fromTable(i % sizes.boardW, Math.floor(i / sizes.boardW));
