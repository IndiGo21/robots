'use strict';
let canvas;
let selectedRobotC = null;
let selectedPlayerId = null;
let ownid = null;

let shouldDraw = false;

let game = {
    board: {
        w: 16,
        h: 16,
        data: []
    },
    robots: {},
    target: {
        pos: {
            x: 0,
            y: 0
        },
        color: 'rgba(0, 0, 0, 0)'
    },
    timer: {
        startTime: null,
        waitTime: 60
    },
    state: 0
};

let lerpedRobots = {};

let players = {};

let sizes = {
    w: null,
    h: null,
    boardW: null,
    boardH: null,
    cellSide: null,
    strokeW: 1,
    strokeW_bold: 1,
    robotR: null,
    targetR: null
};

function canMove() {
    return game.state === 2 && ownid === selectedPlayerId;
}