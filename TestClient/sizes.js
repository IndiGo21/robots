'use strict';
function onWindowResized() {
    let size = Math.min(windowWidth, windowHeight);
    size *= 0.95;
    resizeCanvas(size + 1, size + 1);
    centerCanvas();
    recalcSizes(size, size);
}

function centerCanvas() {
    var x = (windowWidth - width) / 2;
    var y = (windowHeight - height) / 2;
    canvas.position(x, y);
}

function recalcSizes(width, height) {
    sizes.boardW = game.board.w;
    sizes.boardH = game.board.h;
    sizes.cellSide = Math.min(width / sizes.boardW, height / sizes.boardH);
    sizes.w = sizes.boardW * sizes.cellSide;
    sizes.h = sizes.boardH * sizes.cellSide;
    sizes.strokeW = Math.max(1, sizes.cellSide / 50);
    sizes.strokeW_bold = Math.max(1, sizes.cellSide / 25);
    sizes.robotR = Math.max(sizes.cellSide - 2, sizes.cellSide * 0.8);
    sizes.robotR = Math.max(sizes.cellSide - 3, sizes.cellSide * 0.2);
}