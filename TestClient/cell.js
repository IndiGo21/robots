'use strict';
class Cell {
    constructor(upperLeft_x, upperLeft_y) {
        this.upperLeft = {
            x: upperLeft_x,
            y: upperLeft_y
        };
    }

    static fromTable(x, y) {
        return new Cell(x * sizes.cellSide, y * sizes.cellSide);
    }

    get center() {
        return {
            x: this.upperLeft.x + sizes.cellSide / 2,
            y: this.upperLeft.y + sizes.cellSide / 2
        };
    }
    set center(value) {
        this.upperLeft.x = value.x - sizes.cellSide / 2;
        this.upperLeft.y = value.y - sizes.cellSide / 2;
    }

    get upperRight() {
        return {
            x: this.upperLeft.x + sizes.cellSide,
            y: this.upperLeft.y
        };
    }
    get lowerLeft() {
        return {
            x: this.upperLeft.x,
            y: this.upperLeft.y + sizes.cellSide
        };
    }
    get lowerRight() {
        return {
            x: this.upperLeft.x + sizes.cellSide,
            y: this.upperLeft.y + sizes.cellSide
        };
    }
    get table() {
        return {
            x: Math.floor(this.upperLeft.x / sizes.cellSide),
            y: Math.floor(this.upperLeft.y / sizes.cellSide)
        };
    }
}