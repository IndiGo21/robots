/// <reference path="./ref/p5.global-mode.d.ts" />
'use strict';

function setup() {
    canvas = createCanvas(100, 100);
    canvas.parent('canvasHolder');
    onWindowResized();
    noFill();
    strokeWeight(sizes.strokeW);
};

function draw() {
    if (!shouldDraw) { return; }
    background(0);
    drawGrid();
    drawWalls();
    drawTimer();
    lerpRobots();
    drawRobots();
    drawTarget();
};

function windowResized() {
    if (!shouldDraw) { return; }
    onWindowResized();
}

function mousePressed() {
    if (!shouldDraw) { return; }
    onMousePressed();
}

function mouseDragged() {
    if (!shouldDraw) { return; }
    onMouseDragged();
}
