'use strict';
const onDrawWall = {
    _line: (a, b) => line(a.x, a.y, b.x, b.y),
    left: function (cell) {
        stroke(255);
        this._line(cell.upperLeft, cell.lowerLeft);
    },
    right: function (cell) {
        stroke(255);
        this._line(cell.upperRight, cell.lowerRight);
    },
    top: function (cell) {
        stroke(255);
        this._line(cell.upperLeft, cell.upperRight);
    },
    bottom: function (cell) {
        stroke(255);
        this._line(cell.lowerLeft, cell.lowerRight);
    },
};

function onDrawRobot(cell, color) {
    let center = cell.center;
    stroke(color);
    strokeWeight(sizes.strokeW_bold);
    ellipse(center.x, center.y, sizes.cellSide * 0.8);
}

// function onDrawSelectedRobot(cell, color) {
//     let center = cell.center;
//     stroke(color);
//     strokeWeight(sizes.strokeW_bold);
//     ellipse(center.x, center.y, sizes.cellSide * 0.8,);
//     strokeWeight(sizes.strokeW);
//     ellipse(center.x, center.y, sizes.cellSide * 0.65);
// }

function onDrawSelectedRobot(cell, color) {
    if (!canMove()) {
        return onDrawRobot.apply(this, arguments);
    }
    let center = cell.center;
    stroke(selectedRobotC);
    strokeWeight(sizes.strokeW_bold);
    let minY = min(sizes.cellSide * 0.8, max(sizes.cellSide * (1 / (abs(mouseY - center.y))) * 50, 14));
    let minX = min(sizes.cellSide * 0.8, max(sizes.cellSide * (1 / (abs(mouseX - center.x))) * 50, 14));
    if (minX < minY) {
        ellipse(center.x, center.y, minX, sizes.cellSide * 0.8);
    }
    else {
        ellipse(center.x, center.y, sizes.cellSide * 0.8, minY);
    }
    strokeWeight(sizes.strokeW);
}

function onDrawTarget(cell) {
    let center = cell.center;
    stroke(game.target.color);
    ellipse(center.x, center.y, sizes.cellSide * 0.2);
}
