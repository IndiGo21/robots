'use strict';
let updateMsgHandler = {};

updateMsgHandler['timerstarted'] = function (msg) {
    game.timer.startTime = millis();
};
updateMsgHandler['timerstopped'] = function (msg) {
    game.timer.startTime = null;
};
updateMsgHandler['timerfull'] = function (msg) {
    game.timer.waitTime = msg[1];
    if (msg[2]) {
        game.timer.startTime = millis() - (game.timer.waitTime - msg[3]) * 1000;
    } else {
        game.timer.startTime = null;
    }
};
updateMsgHandler['startround'] = function (msg) {
    selectedPlayerId = null;
    for (let id in players) {
        delete players[id].bet;
    }
    redrawScoreboard();
};

updateMsgHandler['delplayer'] = function (msg) {
    delete players[msg[1]];
    redrawScoreboard();
};
updateMsgHandler['turnstart'] = function (msg) {
    selectedPlayerId = msg[1];
    redrawScoreboard();
};
updateMsgHandler['board'] = function (msg) {
    game.board = msg[1];
};
updateMsgHandler['robot'] = function (msg) {
    game.robots[msg[1]] = msg[2];
};
updateMsgHandler['robots'] = function (msg) {
    game.robots = msg[1];
};
updateMsgHandler['target'] = function (msg) {
    game.target = msg[1];
};
updateMsgHandler['newplayer'] = function (msg) {
    players[msg[1]] = msg[2];
    redrawScoreboard();
};
updateMsgHandler['playerchanged'] = updateMsgHandler['newplayer'];
updateMsgHandler['newbet'] = function (msg) {
    players[msg[1]].bet = msg[2];
    redrawScoreboard();
};
updateMsgHandler['state'] = function (msg) {
    game.state = msg[1];
};
updateMsgHandler['ownid'] = function (msg) {
    ownid = msg[1];
};
updateMsgHandler['initdone'] = function (msg) {
    lerpedRobots = { ...game.robots };
};