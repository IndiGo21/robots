#include <iostream>
#include <string>

#include "../Server/libs/json.h"

using namespace std;
#define dp(var) (std::cout << #var ": " << (var) << std::endl) //�������, ������� ���������� �������� ���������� ���������� � �� ��������
#define as(s) (_assert((s), #s)) //����������� ����� ������� ������

void _assert(bool b, const char* info) {
    if (!b) {
        std::cout << "Failed on " << info << std::endl; //������� ��������� �� ������, ���� ���������� �������� ������
    }
}

struct MyClass { //��������� ��� ������� ������ ������
    int i = 1;
    float f = 2;
};

std::unique_ptr<JSON_Value> to_json(const MyClass& v) {  //������ ���������� ������� to_json ��� ������������� ������ MyClass
    auto ptr = std::make_unique<JSON_Object>();
    JSON_Object& json = *ptr;
    json.pushNumber("i", v.i);
    json.pushNumber("f", v.f);
    return ptr;
}

/*
void foo(const JSON_Object& json) { //��� �������� ��������� �� ���������� � ������� JSON_Object
   as(json.getObject("MyObj").getType()=="object");
   as(json.getNumber("i")==-123);
   as(json.getArray("lets").getString(2)=="c");
}*/


int main() {
    std::string_view s1 = "{\"firstName\":\"John\",\"lastName\":\"Smith\",\"isAlive\":true,\"age\":27,\"address\":{\"streetAddress\":\"212ndStreet\",\"city\":\"NewYork\",\"state\":\"NY\",\"postalCode\":\"10021-3100\"},\"phoneNumbers\":[{\"type\":\"home\",\"number\":\"212555-1234\"},{\"type\":\"office\",\"number\":\"646555-4567\"}],\"children\":[],\"spouse\":null}";
    as(JSON_Value::typeof(s1)=="object"); //�������� �������� �� ���� ������ ��������, ���� ���  - ������� ���������
    JSON_Object parsed1 = JSON_Value::ParseObject(s1); //������� �� ������ ������
    as(parsed1.str()=="{\"address\":{\"city\":\"NewYork\",\"postalCode\":\"10021-3100\",\"state\":\"NY\",\"streetAddress\":\"212ndStreet\"},\"age\":27,\"children\":[],\"firstName\":\"John\",\"isAlive\":true,\"lastName\":\"Smith\",\"phoneNumbers\":[{\"number\":\"212555-1234\",\"type\":\"home\"},{\"number\":\"646555-4567\",\"type\":\"office\"}],\"spouse\":null}");


    std::string_view s2 = "[1, true, {\"PI\":3.14}]"; //������ ������� � ����������
    as(JSON_Value::typeof(s2)=="array");  //���������, ������ �� ������
    JSON_Array parsed2 = JSON_Value::ParseArray(s2); //���������� ������� ParseArray ��� ����, ����� ���������
    as(parsed2.str()=="[1,true,{\"PI\":3.14}]"); // �������� �� ��� ��� �����

    as(JSON_Value::ParseNumber("123").v==123); // .v ���������� ��������. ��������� ��������� �� ��������
    as(JSON_Value::ParseNumber("-1.23").v==-1.23);
    as(JSON_Value::ParseNumber("-1.2e1").v==-1.2e1);
    as(JSON_Value::ParseNumber("-1.2e+1").v==-1.2e+1);
    as(JSON_Value::ParseNumber("-1.2e-1").v==-1.2e-1);
    as(JSON_Value::ParseNumber("0").v==0);

	JSON_Object json; //��������� ������ ����� � ����������� �������
	json.pushBool("b", true);
	json.pushNumber("i", -123);
	json.pushString("s", "abc");
	json.pushArray("nums", std::array{1, 2, 3, 4, 5});
	json.pushArray("lets", std::array{"a", "b", "c"});
	json.pushObject("MyObj", MyClass());
	json.pushArray("ObjArr", std::array{MyClass(), MyClass()});
	json.openObject("Math"); //��������� � ����� ������
	{
		json.pushNumber("PI", 3.14); //
		json.openObject("Adv"); //��������� {
		{
		    json.pushBool("hard", true);
		}
		json.closeObject("Adv"); //}
	}
	json.closeObject();

	as(json.str()=="{\"Math\":{\"Adv\":{\"hard\":true},\"PI\":3.14},\"MyObj\":{\"f\":2,\"i\":1},\"ObjArr\":[{\"f\":2,\"i\":1},{\"f\":2,\"i\":1}],\"b\":true,\"i\":-123,\"lets\":[\"a\",\"b\",\"c\"],\"nums\":[1,2,3,4,5],\"s\":\"abc\"}");

    as(json.getObject("Math").getObject("Adv").getBool("hard")==1);//��������� �� ������������ ��������
    json.getObject("Math").getObject("Adv").getBool("hard") = 0; //�������� �������� hard �� 0
    as(json.getObject("Math").str()=="{\"Adv\":{\"hard\":false},\"PI\":3.14}"); //���������, ���������� ��

    as(json.typeof("b")=="boolean");
    as(json.typeof("nums")=="array");

   // foo(json);
}
