#pragma once
#include <map>
#include "libs/Table.h"
#include <random>
#include <stack>

class GameField {
public:
    GameField();
    std::mt19937 rng;
public:  //Declarations
	enum RobotColor { Red = 0, Green, Blue, Yellow, Silver };
	static constexpr std::array colorNames = { "red", "green", "blue", "yellow", "silver" };
	static constexpr RobotColor colorStrToEnum(std::string_view s);
	struct BoardCell {
        enum : uint8_t { Left = 1, Top = 2, Right = 4, Bottom = 8 };
        uint8_t borders;
    };
	typedef Vec2 Robot;
	struct Target {
        Vec2 pos;
        RobotColor color;
    };
public:  //Data
	table<BoardCell> board = {16, 16};
	Target target;
	std::map<RobotColor, Robot> robots;
	std::map<RobotColor, Robot> robots_bkp;
	std::stack<std::pair<RobotColor, Robot>> move_history;
public:  //Target functions
	void moveTarget();
	bool onTarget(const RobotColor&) const;
public:  //Robot functions
    bool hasRobot(const Vec2& pos) const;
	bool moveRobot(const RobotColor& color, Vec2 direction);
	void saveRobots() { robots_bkp = robots; }
	void loadRobots() { robots = robots_bkp; }
	RobotColor undo();
	void clearhistory();
};



constexpr GameField::RobotColor GameField::colorStrToEnum(std::string_view s) {
    for (size_t i = 0; i < colorNames.size(); i++) {
        if (colorNames[i] == s) {
            return (RobotColor) i;
        }
    }
    return (RobotColor) -1;
}
