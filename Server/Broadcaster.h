#pragma once
#include <map>
#include <mutex>
#include "libs/Websocket.h"
#include <iostream>


class WebsocketBroadcaster {
public:
    using conn_id = size_t;
    static constexpr conn_id INVALID_ID = (conn_id) -1;
private:
    std::map<conn_id, WebsocketConnection> socks;
    std::mutex m;
    conn_id nextid = 0;
public:
    conn_id addConnection(WebsocketConnection&& sock) {
        std::lock_guard lg(m);
        socks.emplace_hint(socks.end(), nextid++, std::move(sock));
        return nextid - 1;
    }

    WebsocketConnection& getConnection(conn_id id) {
        return socks.at(id);
    }

    bool removeConnection(conn_id id) {
        std::lock_guard lg(m);
        return socks.erase(id);
    }

    template <typename... Ts>
    void broadcast(Ts&&... args) {
        std::lock_guard lg(m);
        for (auto& e_pair : socks) {
            try {
                e_pair.second.Send(std::forward<Ts>(args)...);
            } catch (const SockError& e) {
                std::cout << "Broadcaster SockError: " << e.what() << std::endl;
            }
        }
    }
};
