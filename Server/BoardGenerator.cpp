#include "BoardGenerator.h"
#include "libs/Matrix.h"

using Matr8 = MatrixS<8, 8, int>;
using Matr16 = MatrixS<16, 16, int>;
using Matr8pair = std::array<Matr8*, 2>;

namespace {

    template <size_t A, typename T>
    constexpr MatrixS<A, A, T> rotateB_90(const MatrixS<A, A, T>& m) {
        MatrixS<A, A, T> m2;
        for (size_t r = 0; r < A; r++) {
            for (size_t c = 0; c < A; c++) {
                m2(r, c) = m(A - c - 1, r);
            }
        }
        return m2;
    }


    Matr8 rotate90(const Matr8& m) {
        Matr8 m2 = rotateB_90(m);
        for (auto& e : m2) {
            e <<= 1;
            e |= e >> 4;
            e &= 15;
        }
        return m2;
    }


    MatrixS<16, 16, int> merge_board(const Matr8& m1, Matr8 m2, Matr8 m3, Matr8 m4) {
        m2 = rotate90(m2);
        m3 = rotate90(rotate90(rotate90(m3)));
        m4 = rotate90(rotate90(m4));
        MatrixS<16, 16, int> m;
        for (size_t i = 0; i < 8; i++) {
            for (size_t j = 0; j < 8; j++) {
                m(i, j) = m1(i, j);
                m(i, j+8) = m2(i, j);
                m(i+8, j) = m3(i, j);
                m(i+8, j+8) = m4(i, j);
            }
        }
        return m;
    }

    template <typename URBG>
    Matr16 _generate(URBG&& rng) {
        Matr8 matr_1a = {{
            0, 0, 0, 0, 4, 0, 0, 0,
            0, 0,12, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 9, 0, 0, 0, 0, 0, 0,
            8, 0, 0, 0, 0, 0, 3, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 6, 0, 0,
            0, 0, 0,12, 0, 0, 0, 3,
        }};
        Matr8 matr_2a = {{
			0, 0, 0, 0, 4, 0, 0, 0,
			0, 0, 0, 0, 0, 0,12, 0,
			0, 3, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0,
			8, 0, 0, 0, 0, 0, 6, 0,
			0, 0, 0, 9, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 3,
		}};
        Matr8 matr_3a = {{
			0, 0, 0, 4, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0,12, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0,
			8, 0, 6, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 9,
			0, 3, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 3,
		}};
        Matr8 matr_4a = {{
			0, 0, 0, 0, 4, 0, 0, 0,
			0, 0, 3, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 9, 0,
			8, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 6, 0, 0, 0,
			0,12, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 3,
		}};

        Matr8 matr_1b = {{
            0, 0, 0, 4, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 9, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 6, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 3, 0, 0,
            0, 0,12, 0, 0, 0, 0,12,
            8, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 3,
        }};
        Matr8 matr_2b = {{
            0, 4, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 3, 0, 0, 0,
            0, 6, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0,12, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            2, 0, 0, 9, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 3,
        }};
        Matr8 matr_3b = {{
            0, 0, 0, 4, 0, 0, 0, 0,
            0, 9, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 6, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0,12, 0, 0, 0, 0, 0,
            8, 0, 0, 0, 0, 0, 0, 3,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 3,
        }};
        Matr8 matr_4b = {{
            0, 0, 0, 4, 0, 0, 0, 0,
            0, 0, 0, 0, 0,12, 0, 0,
            0, 9, 0, 0, 0, 0, 0, 0,
            8, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 3, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 6, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 3,
        }};

        std::array<Matr8pair, 4> board_data = {
            Matr8pair { &matr_1a, &matr_1b },
            Matr8pair { &matr_2a, &matr_2b },
            Matr8pair { &matr_3a, &matr_3b },
            Matr8pair { &matr_4a, &matr_4b },
        };

        std::shuffle(board_data.begin(), board_data.end(), std::forward<URBG>(rng));
        std::uniform_int_distribution<> rnd_b(0, 1);
        //return merge_board(*board_data[0][rnd_b(rng)], *board_data[1][rnd_b(rng)], *board_data[2][rnd_b(rng)], *board_data[3][rnd_b(rng)]);
        return merge_board(matr_1a, matr_2a, matr_3a, matr_4a);
    }
}

table<GameField::BoardCell> generateBoard(std::mt19937& gen) {
    Matr16 m = _generate(gen);
    table<GameField::BoardCell> t {16, 16};
    for (size_t i = 0; i < 16*16; i++) {
        t[i].borders = m[i];
    }
    return t;
}
