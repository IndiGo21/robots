#pragma once
#include <memory>
#include "libs/json.h"
#include "GameField.h"
#include "Player.h"


//Optimized version
//std::unique_ptr<JSON_Value> to_json(const GameField::BoardCell& cell) {
//    auto ptr = std::make_unique<JSON_Object>();
//	  JSON_Object& json = *ptr;
//    json.pushNumber("borders", cell.borders);
//    return ptr;
//}

template <typename T>
std::unique_ptr<JSON_Value> to_json(const table<T>& t) {
    auto ptr = std::make_unique<JSON_Object>();
	JSON_Object& json = *ptr;
    json.pushNumber("w", t.w);
    json.pushNumber("h", t.h);
    json.pushArray("data", t.data, t.data + t.n);
    return ptr;
}

std::unique_ptr<JSON_Value> to_json(const Vec2& vec2);
std::unique_ptr<JSON_Value> to_json(const GameField::BoardCell& cell);
std::unique_ptr<JSON_Value> to_json(const Player& p);
std::unique_ptr<JSON_Value> to_json(const decltype(GameField::robots)& robots);
std::unique_ptr<JSON_Value> to_json(const GameField::Target& target);
std::unique_ptr<JSON_Value> to_json(const GameField& game);
std::unique_ptr<JSON_Value> to_json(const std::map<Player::ID, Player>& v);
