#pragma once
#include "Broadcaster.h"
#include "GameLogic.h"

class RobotsServer {
    class ClientThread;
    WebsocketBroadcaster bcast;
    GameLogic game;
public:
    RobotsServer();
    [[noreturn]]
    void start();
};


class RobotsServer::ClientThread {
    Player::ID player_id = Player::INVALID_ID;
    WebsocketBroadcaster::conn_id sock_id = WebsocketBroadcaster::INVALID_ID;
    GameLogic& game;
    WebsocketBroadcaster& bcast;
    ClientThread(WebsocketBroadcaster& bc, GameLogic& game) : game(game), bcast(bc) { }
public:
    ///Both references must remain valid while object exists
    ClientThread(ConnectedClient&&, WebsocketBroadcaster&, GameLogic&);
    ~ClientThread();
};
