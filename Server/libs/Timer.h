#pragma once
#include <chrono>
#include <thread>

using namespace std::literals::chrono_literals;

class Timer {
    typedef std::chrono::steady_clock clock;
    clock::time_point trigger;
public:
    ///Use literals like 1h, 1min, 1s, 1ms, etc.
    template <typename D>
    void Set(const D& delta) { trigger = clock::now() + delta; }
    void Wait() { std::this_thread::sleep_until(trigger); }
    bool Check() { return clock::now() >= trigger; }
};
