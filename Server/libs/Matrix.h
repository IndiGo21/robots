#pragma once
#include <initializer_list>
#include <algorithm>
#include <iomanip>
#include <array>

template <size_t _rows, size_t _cols, typename T = float>
class MatrixS {
  public:
	static constexpr size_t rows = _rows;
	static constexpr size_t cols = _cols;
	static constexpr size_t n = rows * cols;
  public:
    std::array<T, rows * cols> data;
  public:

	constexpr MatrixS() : data() {}
	constexpr MatrixS(std::array<T, n> data) : data(data) {}
	constexpr MatrixS(const MatrixS<rows, cols, T>& other) : data(other.data) {}
	template <typename _T>
	MatrixS(const MatrixS<rows, cols, _T>& other) {
		for(size_t i = 0; i < n; i++) {
			data[i] = other[i];
		}
	}
	constexpr MatrixS(MatrixS<rows, cols, T>&& other) : data(std::move(other.data)) {}
	MatrixS<rows, cols, T>& operator=(const MatrixS<rows, cols, T>& other)& { data = other.data; return *this; }
	MatrixS<rows, cols, T>& operator=(MatrixS<rows, cols, T>&& other)& { data = std::move(other.data); return *this; }

	constexpr T& operator[](int i) { return data[i]; }
	constexpr T operator[](int i) const { return data[i]; }
	constexpr T& operator()(int row, int col) { return data[row * cols + col]; }
	constexpr T operator()(int row, int col) const { return data[row * cols + col]; }

	template <size_t rows, size_t cols, typename _T>
	friend std::ostream& operator<<(std::ostream&, const MatrixS<rows, cols, _T>&);

	constexpr auto begin() { return data.begin(); }
	constexpr auto end() { return data.end(); }
	constexpr const auto begin() const { return data.begin(); }
	constexpr const auto end() const { return data.end(); }
	void fill(T v) { std::fill(data.begin(), data.end(), v); }

	inline size_t rOf(size_t i) const { return i / cols; }
	inline size_t cOf(size_t i) const { return i % cols; }

	template <typename _T>
	MatrixS<rows, cols, T>& operator+=(const MatrixS<rows, cols, _T>& other)& {
		for (size_t i = 0; i < n; i++) {
			data[i] += other.data[i];
		}
		return *this;
	}
	template <typename _T>
	MatrixS<rows, cols, T>& operator-=(const MatrixS<rows, cols, _T>& other)& {
		for (size_t i = 0; i < n; i++) {
			data[i] += other.data[i];
		}
		return *this;
	}
	MatrixS<rows, cols, T>& operator*=(const T other)& {
		for (size_t i = 0; i < n; i++) {
			data[i] *= other;
		}
		return *this;
	}
	MatrixS<rows, cols, T>& operator/=(const T other)& {
		for (size_t i = 0; i < n; i++) {
			data[i] /= other;
		}
		return *this;
	}

	MatrixS<rows, cols, T> operator-() const {
		MatrixS<rows, cols, T> ret;
		for (size_t i = 0; i < n; i++) {
			ret.data[i] = -this->data[i];
		}
		return ret;
	}

	template <size_t cols2, typename _T>
	MatrixS<rows, cols2, T> operator*(const MatrixS<cols, cols2, _T>& other) const {
		MatrixS<rows, cols2, T> ret;
		for (size_t i = 0; i < ret.n; i++) {
			const int cRow = ret.rOf(i);
			const int cCol = ret.cOf(i);
			T sum = 0;
			for (size_t j = 0; j < cols; j++) {
				sum += (*this)(cRow, j) * other(j, cCol);
			}
			ret[i] = sum;
		}
		return ret;
	}

	const MatrixS<rows, cols, T> Transposed() const {
		MatrixS<cols, rows, T> ret;
		for (size_t i = 0; i < rows; i++) {
			for (size_t j = 0; j < cols; j++) {
				ret.data(j, i) = this->data(i, j);
			}
		}
		return ret;
	}

	template <typename RET = T>
	RET Trace() const {
		static_assert(rows == cols, "Trace of a non-square matrix is undefined");
		RET sum = 0;
		for (size_t i = 0; i < rows; i++) {
			sum += data[(rows + 1) * i];
		}
		return sum;
	}

	static constexpr MatrixS<rows, cols, T> Zero() {
		constexpr std::array<T, rows* cols> a = {0};
		return MatrixS(a);
	}
	static constexpr MatrixS<rows, cols, T> Identity() {
		static_assert(rows == cols, "Identity matrix must be square");
		auto ret = MatrixS<rows, cols, T>::Zero();
		for (size_t i = 0; i < rows; i++) {
			ret.data[(rows + 1)*i] = 1;
		}
		return ret;
	}
};

template <size_t rows, size_t cols, typename T, typename _T>
MatrixS<rows, cols, T> operator+(MatrixS<rows, cols, T> a, const MatrixS<rows, cols, _T>& b) {
	return a += b;
}
template <size_t rows, size_t cols, typename T, typename _T>
MatrixS<rows, cols, T> operator-(MatrixS<rows, cols, T> a, const MatrixS<rows, cols, _T>& b) {
	return a += b;
}
template <size_t rows, size_t cols, typename T>
MatrixS<rows, cols, T> operator*(MatrixS<rows, cols, T> a, T b) {
	return a *= b;
}
template <size_t rows, size_t cols, typename T>
MatrixS<rows, cols, T> operator/(MatrixS<rows, cols, T> a, T b) {
	return a /= b;
}


template <size_t rows, size_t cols, typename T>
std::ostream& operator<<(std::ostream& os, const MatrixS<rows, cols, T>& m) {
	static const auto len = [](const T a, const auto& os) {
		std::stringstream ss;
		ss.flags(os.flags());
		ss << a;
		return ss.str().size();
	};

	if (rows * cols > 0) {

		auto mlen = len(m.data[0], os);
		for (const auto& v : m.data) {
			mlen = std::max(mlen, len(v, os));
		}
		for (size_t row = 0; row < rows; row++) {
			for (size_t col = 0; col < cols - 1; col++) {
				os << std::setw(mlen) << m(row, col) << ' ';
			}
			os << std::setw(mlen) << m(row, cols - 1) << std::endl;
		}
	}
	return os;
}




template <typename T>
class MatrixD {
  public:
	size_t rows;
	size_t cols;
	size_t n;
	T* data;


	MatrixD() : rows(0), cols(0), n(0), data(nullptr) {}
	~MatrixD() { delete[] data; }
	MatrixD(size_t rows, size_t cols) : rows(rows), cols(cols), n(rows * cols), data(n ? new T[n] : nullptr) {}
	MatrixD(size_t rows, size_t cols, const T* data)
		: MatrixD(rows, cols) { std::copy(data, data + n, this->data); }
	MatrixD(size_t rows, size_t cols, T*&& data)
		: rows(rows), cols(cols), n(rows * cols) {
		this->data = data;
		data = nullptr;
	}
	MatrixD(const MatrixD<T>& other) : MatrixD(other.rows, other.cols) {
		std::copy(other.data, other.data + n, this->data);
	}
	template <typename _T>
	MatrixD(const MatrixD<_T>& other) : MatrixD(other.rows, other.cols) {
		for (size_t i = 0; i < other.n; i++) {
			data[i] = other.data[i];
		}
	}
	MatrixD(MatrixD<T>&& other) : rows(other.rows), cols(other.cols), n(other.n) {
		data = other.data;
		other.data = nullptr;
	}

	MatrixD<T>& operator=(const MatrixD<T>& other)& {
		rows = other.rows;
		cols = other.cols;
		if (n != other.n) {
			n = other.n;
			delete[] data;
			data = new T[n];
		}
		std::copy(other.data, other.data + n, this->data);
		return *this;
	}
	MatrixD<T>& operator=(MatrixD<T>&& other)& {
		rows = other.rows;
		cols = other.cols;
		std::swap(n, other.n);
		std::swap(data, other.data);
		return *this;
	}

	inline T& operator()(int row, int col) { return data[row * cols + col]; }
	inline T operator()(int row, int col) const { return data[row * cols + col]; }

	inline T* begin() { return data; }
	inline T* end() { return data + n; }
	inline const T* begin() const { return &data[0]; }
	inline const T* end() const { return &data[n]; }
	void fill(T v) { std::fill(data, data + n, v); }

	inline size_t rOf(size_t i) const { return i / cols; }
	inline size_t cOf(size_t i) const { return i % cols; }

	template <typename _T>
	friend std::ostream& operator<<(std::ostream&, const MatrixD<_T>&);

	///The rhs matrix can be smaller
	template <typename _T>
	MatrixD<T>& operator+=(const MatrixD<_T>& other)& {
		for (size_t i = 0; i < other.n; i++) {
			data[i] += other.data[i];
		}
		return *this;
	}
	///The rhs matrix can be smaller
	template <typename _T>
	MatrixD<T>& operator-=(const MatrixD<_T>& other)& {
		for (size_t i = 0; i < other.n; i++) {
			data[i] -= other.data[i];
		}
		return *this;
	}
	MatrixD<T>& operator*=(const T v)& {
		for (auto && e : *this) { e *= v; }
		return *this;
	}
	MatrixD<T>& operator/=(const T v)& {
		for (auto && e : *this) { e /= v; }
		return *this;
	}

	template <typename _T>
	MatrixD<T> operator*(const MatrixD<_T>& other) const {
		MatrixD<T> ret {this->rows, other.cols};
		for (size_t i = 0; i < ret.n; i++) {
			const size_t cRow = ret.rOf(i);
			const size_t cCol = ret.cOf(i);
			T sum = 0;
			for (size_t j = 0; j < cols; j++) {
				sum += (*this)(cRow, j) * other(j, cCol);
			}
			ret.data[i] = sum;
		}
		return ret;
	}
	template <typename _T>
	MatrixD<T>& operator*=(const MatrixD<_T>& other)& {
		return *this = *this * other;
	}

	MatrixD<T> operator-() const {
		MatrixD<T> ret {rows * cols};
		for (size_t i = 0; i < n; i++) {
			ret.data[i] = -this->data[i];
		}
		return ret;
	}

	MatrixD<T> Transposed() const {
		MatrixD<T> ret {cols, rows};
		for (size_t i = 0; i < rows; i++) {
			for (size_t j = 0; j < cols; j++) {
				ret.data(j, i) = this->data(i, j);
			}
		}
		return ret;
	}

	inline void Transpose()& {
		*this = this->Transposed();
	}

	template <typename RET = T>
	RET Trace() const {
		RET sum = 0;
		for (int i = 0; i < rows; i++) {
			sum += data[(rows + 1) * i];
		}
		return sum;
	}

	static constexpr MatrixD<T> Identity(size_t side) {
		MatrixD<T> ret {side, side};
		ret.fill(0);
		for (size_t i = 0; i < side; i++) {
			ret.data[(side + 1)*i] = 1;
		}
		return ret;
	}
};


template <typename T>
std::ostream& operator<<(std::ostream& os, const MatrixD<T>& m) {
	static const auto len = [](const T a, const auto& os) {
		std::stringstream ss;
		ss.flags(os.flags());
		ss << a;
		return ss.str().size();
	};

	if (m.n) {

		auto mlen = len(m.data[0], os);
		for (const auto& v : m) {
			mlen = std::max(mlen, len(v, os));
		}
		for (size_t row = 0; row < m.rows; row++) {
			for (size_t col = 0; col < m.cols - 1; col++) {
				os << std::setw(mlen) << m(row, col) << ' ';
			}
			os << std::setw(mlen) << m(row, m.cols - 1) << std::endl;
		}
	}
	return os;
}
