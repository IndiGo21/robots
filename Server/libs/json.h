#pragma once
#include <map>
#include <vector>
#include <stack>
#include <string>
#include <string_view>
#include <sstream>
#include <memory>

class JSON_Number;
class JSON_String;
class JSON_Object;
class JSON_Array;
class JSON_Bool;
class JSON_Null;

class JSON_Value {
public:
    virtual std::string str() const = 0;
    virtual std::string_view getType() const = 0;

    class exception : public std::out_of_range {
        using std::out_of_range::out_of_range;
        using std::out_of_range::operator=;
    };

    static std::string_view typeof(std::string_view str);
    static std::unique_ptr<JSON_Value> ParseAny(std::string_view str);
    static JSON_Object ParseObject(std::string_view str);
    static JSON_Number ParseNumber(std::string_view str);
    static JSON_String ParseString(std::string_view str);
    static JSON_Array ParseArray(std::string_view str);
    static JSON_Bool ParseBool(std::string_view str);
    static JSON_Null ParseNull(std::string_view str);
private:
    class parser;
    static void checkType(std::string_view str, std::string_view type) {
        if (typeof(str) != type) { throw exception("JSON: type mismatch"); }
    }
};

inline std::unique_ptr<JSON_Value> to_json(std::unique_ptr<JSON_Value> v) { return std::move(v); }

class JSON_Number : public JSON_Value {
public:
    explicit JSON_Number(double v) : v(std::move(v)) { }
    double v;
    std::string str() const override {
        std::ostringstream ss;
        ss << v;
        return ss.str();
    }
    std::string_view getType() const override { return "number"; }
};

class JSON_Null : public JSON_Value {
public:
    std::string str() const override {
        return "null";
    }
    std::string_view getType() const override { return "null"; }
};

template <typename T>
inline typename std::enable_if<std::is_arithmetic<T>::value, std::unique_ptr<JSON_Value>>::type
to_json(T v) { return std::make_unique<JSON_Number>(v); }

class JSON_String : public JSON_Value {
public:
    explicit JSON_String(std::string v) : v(std::move(v)) { }
    std::string v;
    std::string str() const override {
        std::string s = "\"";
        s.reserve(v.size() + 16);
        for (size_t i = 0; i < v.size(); i++) {
            if (v[i] == '\b') {
                s += "\\b";
            } else if (v[i] == '\f') {
                s += "\\f";
            } else if (v[i] == '\n') {
                s += "\\n";
            } else if (v[i] == '\r') {
                s += "\\r";
            } else if (v[i] == '\t') {
                s += "\\t";
            } else {
                if (v[i] == '\"' || v[i] == '\\' || v[i] == '/') {
                    s.push_back('\\');
                }
                s.push_back(v[i]);
            }
        }
        s.push_back('\"');
        return s;
    }
    std::string_view getType() const override { return "string"; }
};


template <typename T>
inline typename std::enable_if<std::is_convertible<T, std::string>::value, std::unique_ptr<JSON_Value>>::type
to_json(T v) { return std::make_unique<JSON_String>(std::move(v)); }
inline std::unique_ptr<JSON_Value> to_json(std::string_view v) { return std::make_unique<JSON_String>(std::string(v)); }

class JSON_Bool : public JSON_Value {
public:
    explicit JSON_Bool(bool v) : v(std::move(v)) { }
    bool v;
    std::string str() const override {
        std::ostringstream ss;
        ss << std::boolalpha << v;
        return ss.str();
    }
    std::string_view getType() const override { return "boolean"; }
};

inline std::unique_ptr<JSON_Value> to_json(bool v) { return std::make_unique<JSON_Bool>(v); }

class JSON_Object;
class JSON_Array;

inline std::unique_ptr<JSON_Value> to_json(JSON_String v);
inline std::unique_ptr<JSON_Value> to_json(JSON_Number v);
inline std::unique_ptr<JSON_Value> to_json(JSON_Array v);
inline std::unique_ptr<JSON_Value> to_json(JSON_Object v);
inline std::unique_ptr<JSON_Value> to_json(JSON_Bool v);
inline std::unique_ptr<JSON_Value> to_json(JSON_Null v);

template <typename T, typename = std::void_t<> >
struct is_json_map : std::false_type { };

template <typename T>
struct is_json_map<T, std::void_t<
        typename std::enable_if<std::is_convertible<
            decltype(std::declval<T>().begin()->first),
            std::string_view
        >::value, void>::type,
        decltype(std::declval<T>().begin()->second)
    >> : std::true_type
{ };

template <typename Container>
inline typename std::enable_if<is_json_map<Container>::value,
std::unique_ptr<JSON_Value>>::type
to_json(const Container& v);

template <typename Container, typename = std::void_t<
    decltype(std::declval<Container>().begin()),
    decltype(std::declval<Container>().end())
>>
inline typename std::enable_if<!is_json_map<Container>::value,
std::unique_ptr<JSON_Value>>::type
to_json(const Container& v);

class JSON_Array : public JSON_Value {
    std::vector<std::unique_ptr<JSON_Value>> data;
    auto& at(size_t i) {
        if (i >= data.size()) { throw exception("JSON: entry not found"); }
        return data[i];
    }
    const auto& at(size_t i) const {
        if (i >= data.size()) { throw exception("JSON: entry not found"); }
        return data[i];
    }
    template <typename T, typename... Ts>
    static void _forge(JSON_Array& ret, T&& first, Ts&&... others) {
        ret.pushObject(std::forward<T>(first));
        if constexpr (sizeof...(others) > 0) {
            _forge(ret, std::forward<Ts>(others)...);
        }
    }
public:
    JSON_Array() = default;
    template <typename It>
    explicit JSON_Array(It begin, It end) {
        for(It it = begin; it != end; ++it) {
            data.push_back(to_json(*it));
        }
    }
    template <typename Container, typename = std::void_t<
    decltype(std::declval<Container>().begin()),
    decltype(std::declval<Container>().end())>>
    explicit JSON_Array(Container&& v) : JSON_Array(v.begin(), v.end()) { }
    template <typename... Ts>
    static JSON_Array forge(Ts&&... elems) {
        JSON_Array ret;
        _forge(ret, std::forward<Ts>(elems)...);
        return ret;
    }

    JSON_Array& pushNumber(double v) {
        data.emplace_back(std::make_unique<JSON_Number>(v));
        return *this;
    }
    JSON_Array& pushString(std::string v) {
        data.emplace_back(std::make_unique<JSON_String>(std::move(v)));
        return *this;
    }
    JSON_Array& pushBool(bool v) {
        data.emplace_back(std::make_unique<JSON_Bool>(v));
        return *this;
    }
    template <typename... Ts>
    JSON_Array& pushArray(Ts&&... args) {
        data.emplace_back(std::make_unique<JSON_Array>(std::forward<Ts>(args)...));
        return *this;
    }
    JSON_Array& pushObject(JSON_Object&& v) {
        data.emplace_back(std::make_unique<JSON_Object>(std::move(v)));
        return *this;
    }
    template <typename T>
    JSON_Array& pushObject(T&& v) {
        data.emplace_back(to_json(std::forward<T>(v)));
        return *this;
    }
    JSON_Array& pushNull(std::string_view name) {
        data.emplace_back(std::make_unique<JSON_Null>());
        return *this;
    }
    JSON_Array& pushRaw(std::unique_ptr<JSON_Value> v) {
        data.emplace_back(std::move(v));
        return *this;
    }

    double& getNumber(size_t i) {
        auto* e = at(i).get();
        if (e->getType() != "number") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_Number*) e;
        return c->v;
    }
    const double& getNumber(size_t i) const {
        auto* e = at(i).get();
        if (e->getType() != "number") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_Number*) e;
        return c->v;
    }
    std::string& getString(size_t i) {
        auto* e = at(i).get();
        if (e->getType() != "string") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_String*) e;
        return c->v;
    }
    const std::string& getString(size_t i) const {
        auto* e = at(i).get();
        if (e->getType() != "string") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_String*) e;
        return c->v;
    }
    bool& getBool(size_t i) {
        auto* e = at(i).get();
        if (e->getType() != "boolean") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_Bool*) e;
        return c->v;
    }
    const bool& getBool(size_t i) const {
        auto* e = at(i).get();
        if (e->getType() != "boolean") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_Bool*) e;
        return c->v;
    }
    JSON_Array& getArray(size_t i) {
        auto* e = at(i).get();
        if (e->getType() != "array") { throw exception("JSON: type mismatch"); }
        return *(JSON_Array*) e;
    }
    const JSON_Array& getArray(size_t i) const {
        auto* e = at(i).get();
        if (e->getType() != "array") { throw exception("JSON: type mismatch"); }
        return *(JSON_Array*) e;
    }
    JSON_Object& getObject(size_t i) {
        auto* e = at(i).get();
        if (e->getType() != "object") { throw exception("JSON: type mismatch"); }
        return *(JSON_Object*) e;
    }
    const JSON_Object& getObject(size_t i) const {
        auto* e = at(i).get();
        if (e->getType() != "object") { throw exception("JSON: type mismatch"); }
        return *(JSON_Object*) e;
    }
    bool has(size_t i) { return i < data.size(); }
    size_t size() const { return data.size(); }
    bool empty() const { return data.empty(); }
    void clear() { data.clear(); }
    bool erase(size_t i) {
        if (!has(i)) { return false; }
        data.erase(data.begin() + i);
        return true;
    }
    std::string_view typeof(size_t i) const {
        if (i >= data.size()) { return std::string_view(); }
        return data[i]->getType();
    }

    class iterator : public std::vector<std::unique_ptr<JSON_Value>>::iterator {
    public:
        iterator(const std::vector<std::unique_ptr<JSON_Value>>::iterator& v)
            : std::vector<std::unique_ptr<JSON_Value>>::iterator(v)
        { }
        JSON_Value& operator*() {
            return *std::vector<std::unique_ptr<JSON_Value>>::iterator::operator*();
        }
    };
    class const_iterator : public std::vector<std::unique_ptr<JSON_Value>>::const_iterator {
    public:
        const_iterator(const std::vector<std::unique_ptr<JSON_Value>>::const_iterator& v)
            : std::vector<std::unique_ptr<JSON_Value>>::const_iterator(v)
        { }
        const JSON_Value& operator*() {
            return *std::vector<std::unique_ptr<JSON_Value>>::const_iterator::operator*();
        }
    };
    iterator begin() { return (iterator)(data.begin()); }
    iterator end() { return data.end(); }
    const_iterator begin() const { return data.begin(); }
    const_iterator end() const { return data.end(); }

    std::string str() const override {
        std::string s = "[";
        if (!data.empty()) {
            for(const auto& e : data) {
                s += e->str();
                s += ',';
            }
            s.resize(s.size() - 1);
        }
        s += ']';
        return s;
    }
    std::string_view getType() const override { return "array"; }
};

class JSON_Object : public JSON_Value {
    std::map<std::string, std::unique_ptr<JSON_Value>, std::less<>> data;
    std::stack<std::pair<std::string, JSON_Object>> opened;
    auto& getMap() { return opened.empty() ? data : opened.top().second.data; }
    auto& at(std::string_view name) {
        auto& m = data;
        auto it = m.find(name);
        if (it == m.end()) { throw exception("JSON: entry not found"); }
        return it->second;
    }
    const auto& at(std::string_view name) const {
        auto& m = data;
        auto it = m.find(name);
        if (it == m.end()) { throw exception("JSON: entry not found"); }
        return it->second;
    }
    void set_at(std::string_view name, std::unique_ptr<JSON_Value> value) {
        auto& m = getMap();
        auto it = m.find(name);
        if (it == m.end()) { m[std::string(name)] = std::move(value); }
        else { it->second = std::move(value); }
    }
    void set_at(std::string&& name, std::unique_ptr<JSON_Value> value) {
        auto& m = getMap();
        m[std::move(name)] = std::move(value);
    }
    template <typename T1, typename T2, typename... Ts>
    static void _forge(JSON_Object& ret, T1&& first, T2&& second, Ts&&... others) {
        ret.pushObject(std::forward<T1>(first), std::forward<T2>(second));
        if constexpr (sizeof...(others) > 0) {
            _forge(ret, std::forward<Ts>(others)...);
        }
    }
public:
    template <typename... Ts>
    static JSON_Object forge(Ts&&... elems) {
        static_assert(sizeof...(elems) % 2 == 0);
        JSON_Object ret;
        _forge(ret, std::forward<Ts>(elems)...);
        return ret;
    }
    JSON_Object& pushNumber(std::string_view name, double v) {
        set_at(name, std::make_unique<JSON_Number>(v));
        return *this;
    }
    JSON_Object& pushString(std::string_view name, std::string v) {
        set_at(name, std::make_unique<JSON_String>(std::move(v)));
        return *this;
    }
    JSON_Object& pushBool(std::string_view name, bool v) {
        set_at(name, std::make_unique<JSON_Bool>(v));
        return *this;
    }
    template <typename... Ts>
    JSON_Object& pushArray(std::string_view name, Ts&&... args) {
        set_at(name, std::make_unique<JSON_Array>(std::forward<Ts>(args)...));
        return *this;
    }
    JSON_Object& pushObject(std::string_view name, JSON_Object&& v) {
        set_at(name, std::make_unique<JSON_Object>(std::move(v)));
        return *this;
    }
    template <typename T>
    JSON_Object& pushObject(std::string_view name, T&& v) {
        set_at(name, to_json(std::forward<T>(v)));
        return *this;
    }
    JSON_Object& pushNull(std::string_view name) {
        set_at(name, std::make_unique<JSON_Null>());
        return *this;
    }
    JSON_Object& pushRaw(std::string_view name, std::unique_ptr<JSON_Value> v) {
        set_at(name, std::move(v));
        return *this;
    }
    JSON_Object& openObject(std::string_view name) {
        opened.emplace(std::move(name), JSON_Object());
        return *this;
    }
    JSON_Object& closeObject(std::string_view name = std::string_view()) {
        if(opened.empty()) { throw exception("JSON: no objects left to close"); }
        if (!name.empty() && opened.top().first != name) { throw exception("JSON: closing the wrong object"); }
        std::pair<std::string, JSON_Object> t;
        std::swap(t, opened.top());
        opened.pop();
        auto& parent = opened.empty() ? *this : opened.top().second;
        parent.pushObject(std::move(t.first), std::move(t.second));
        return *this;
    }


    double& getNumber(std::string_view name) {
        auto* e = at(name).get();
        if (e->getType() != "number") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_Number*) e;
        return c->v;
    }
    const double& getNumber(std::string_view name) const {
        auto* e = at(name).get();
        if (e->getType() != "number") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_Number*) e;
        return c->v;
    }
    std::string& getString(std::string_view name) {
        auto* e = at(name).get();
        if (e->getType() != "string") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_String*) e;
        return c->v;
    }
    const std::string& getString(std::string_view name) const {
        auto* e = at(name).get();
        if (e->getType() != "string") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_String*) e;
        return c->v;
    }
    bool& getBool(std::string_view name) {
        auto* e = at(name).get();
        if (e->getType() != "boolean") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_Bool*) e;
        return c->v;
    }
    const bool& getBool(std::string_view name) const {
        auto* e = at(name).get();
        if (e->getType() != "boolean") { throw exception("JSON: type mismatch"); }
        auto* c = (JSON_Bool*) e;
        return c->v;
    }
    JSON_Array& getArray(std::string_view name) {
        auto* e = at(name).get();
        if (e->getType() != "array") { throw exception("JSON: type mismatch"); }
        return *(JSON_Array*) e;
    }
    const JSON_Array& getArray(std::string_view name) const {
        auto* e = at(name).get();
        if (e->getType() != "array") { throw exception("JSON: type mismatch"); }
        return *(JSON_Array*) e;
    }
    JSON_Object& getObject(std::string_view name) {
        auto* e = at(name).get();
        if (e->getType() != "object") { throw exception("JSON: type mismatch"); }
        return *(JSON_Object*) e;
    }
    const JSON_Object& getObject(std::string_view name) const {
        auto* e = at(name).get();
        if (e->getType() != "object") { throw exception("JSON: type mismatch"); }
        return *(JSON_Object*) e;
    }
    bool has(std::string_view name) { return data.count(name); }
    size_t size() const { return data.size(); }
    bool empty() const { return data.empty(); }
    void clear() { data.clear(); }
    bool erase(std::string_view name) {
        auto& m = data;
        auto it = m.find(name);
        if (it == m.end()) { return false; }
        data.erase(it);
        return true;
    }
    std::string_view typeof(std::string_view name) const {
        auto& m = data;
        auto it = m.find(name);
        if (it == m.end()) { return std::string_view(); }
        return it->second->getType();
    }

    std::string str() const override {
        if (!opened.empty()) { throw exception("JSON: objects left unclosed"); }
        if (data.empty()) { return "{}"; }
        std::string s = "{";
        for(const auto& e : data) {
            s += '\"';
            s += e.first;
            s += '\"';
            s += ':';
            s += e.second->str();
            s += ',';
        }
        s.resize(s.size() - 1);
        s += '}';
        return s;
    }
    std::string_view getType() const override { return "object"; }
};

template <typename Container>
inline typename std::enable_if<is_json_map<Container>::value,
std::unique_ptr<JSON_Value>>::type
to_json(const Container& v) {
    auto p = std::make_unique<JSON_Object>();
    for (const auto& e : v) {
        p->pushObject(e.first, e.second);
    }
    return p;
}

template <typename Container, typename = std::void_t<
    decltype(std::declval<Container>().begin()),
    decltype(std::declval<Container>().end())
>>
inline typename std::enable_if<!is_json_map<Container>::value,
std::unique_ptr<JSON_Value>>::type
to_json(const Container& v) {
    return std::make_unique<JSON_Array>(v.begin(), v.end());
}






class JSON_Value::parser {
    std::string_view str;
protected:
    friend JSON_Value;
    parser(std::string_view str) : str(str) { }
    std::unique_ptr<JSON_Value> operator()() {
        size_t i = 0;
        return skipValue(i);
    }
private:
    [[noreturn]]
    void throwAt(size_t i, std::string msg) {
        throw JSON_Value::exception(std::move(msg += std::to_string(i)));
    }

    auto& at(size_t i, const char* msg) {
        if (i >= str.size()) { throw exception(msg); }
        return str[i];
    }

    bool isJsonNum(size_t i) {
        return isNumberFirstChar(str[i]);
    }
    size_t isJsonBool(size_t i) {
        if(str.size() - i >= 4) {
            if (str.substr(i, 4) == "true") {
                return 4;
            } else if (str.size() - i >= 5) {
                if (str.substr(i, 5) == "false") {
                    return 5;
                }
            }
        }
        return 0;
    }
    size_t isJsonNull(size_t i) {
        return str.size() - i >= 4 &&
        str.substr(i, 4) == "null";
    }
    bool isNumberChar(char c) {
        return (c >= '0' && c <= '9') ||
        c == '.' || c == '-' || c == '+'
        || c == 'e' || c == 'E';
    }
    bool isNumberFirstChar(char c) {
        return (c >= '0' && c <= '9') || c == '-';
    }
    bool isWs(char c) {
        return c == ' ' || c == '\r'
        || c == '\n' || c == '\t';
    }

    void skipWs(size_t& i) {
        while(i < str.size() && isWs(str[i])) { i++; }
    }
    std::unique_ptr<JSON_Object> skipObject(size_t& i) {
        std::unique_ptr<JSON_Object> ret = std::make_unique<JSON_Object>();
        i++;
        while(true) {
            skipWs(i);
            std::string_view name = skipStringRaw(i);
            skipWs(i);
            if (at(i, "JSON: expected ':', got end") != ':') { throwAt(i, "JSON: expected ':' at "); }
            i++;
            skipWs(i);
            ret->pushRaw(name, skipValue(i));
            skipWs(i);

            char c = at(i, "JSON: expected ',' or '}', got end");
            // c++20 [[likely]]
            if (c == ',') { i++; }
            else if (c == '}') { break; }
            else { throwAt(i, "JSON: expected ',' or '}' at "); }
        }
        i++;
        return ret;
    }
    std::unique_ptr<JSON_Array> skipArray(size_t& i) {
        std::unique_ptr<JSON_Array> ret = std::make_unique<JSON_Array>();
        i++;
        skipWs(i);
        if (at(i, "JSON: expected value or ']', got end") != ']') {
            while(true) {
                skipWs(i);
                ret->pushRaw(skipValue(i));
                skipWs(i);

                char c = at(i, "JSON: expected ',' or ']', got end");
                //likely
                if (c == ',') { i++; }
                else if (c == ']') { break; }
                else { throwAt(i, "JSON: expected ',' or ']' at "); }
            }
        }
        i++;
        return ret;
    }
    std::unique_ptr<JSON_String> skipString(size_t& i) {
        return std::make_unique<JSON_String>(skipStringRaw(i));
    }
    std::string skipStringRaw(size_t& i) {
        std::string ret;
        if (at(i, "JSON: expected string, got end") != '"') { throwAt(i, "JSON: expected string at "); }
        i++;
        while(true) {
            char c = at(i, "JSON: expected '\"', got end");
            if (c == '"') {  //End of string
                i++;
                return ret;
            } else if (c == '\\') {
                i++;
                char c2 = at(i, "JSON: expected escape, got end");
                if (c2 == 'b') {
                    ret += '\b';
                } else if (c2 == 'f') {
                    ret += '\f';
                } else if (c2 == 'n') {
                    ret += '\n';
                } else if (c2 == 'r') {
                    ret += '\r';
                } else if (c2 == 't') {
                    ret += '\t';
                } else if (c2 == 'u') {
                    throwAt(i-1, "JSON: unicode escape not supported. at ");
                } else if (c2 == '\"' || c2 == '\\' || c2 == '/') {
                    ret.push_back(c2);
                } else {  //What to do with an unknown escape?
                    throwAt(i-1, "JSON: unknown escape at ");
                }
            } else {
                ret += c;
            }
            i++;
        }
        return ret;
    }
    std::unique_ptr<JSON_Number> skipNumber(size_t& i) {
        size_t b = i;
        while(i < str.size() && isNumberChar(str[i])) { i++; }
        std::string num = std::string( str.substr(b, i - b) );

        double ret;
        std::istringstream ss(std::move(num));
        ss >> ret;
        if (!ss.eof() || ss.bad() || ss.fail()) {
            throwAt(i, "JSON: malformed number at ");
        }

        return std::make_unique<JSON_Number>(ret);
    }
    std::unique_ptr<JSON_Bool> skipTrue(size_t& i) { i += 4; return std::make_unique<JSON_Bool>(true); }
    std::unique_ptr<JSON_Bool> skipFalse(size_t& i) { i += 5; return std::make_unique<JSON_Bool>(false); }
    std::unique_ptr<JSON_Null> skipNull(size_t& i) { i += 4; return std::make_unique<JSON_Null>(); }
    std::unique_ptr<JSON_Value> skipValue(size_t& i) {
        if (str[i] == '{') {  //Object
            return skipObject(i);
        } else if (str[i] == '[') {  //Array
            return skipArray(i);
        } else if (str[i] == '\"') {  //String
            return skipString(i);
        } else {
            size_t len;
            if (isJsonNum(i)) {  //Number
                return skipNumber(i);
            } else if ((len = isJsonBool(i))) {  //Bool
                if (len == 4) {
                    return skipTrue(i);
                } else {
                    return skipFalse(i);
                }
            } else if ((len = isJsonNull(i))) {  //Null
                return skipNull(i);
            } else {
                throwAt(i, "JSON: unknown value type at ");
            }
        }
    }

    std::string_view getType() {
        const size_t i = 0;
        if (str[i] == '{') {  //Object
            return "object";
        } else if (str[i] == '[') {  //Array
            return "array";
        } else if (str[i] == '\"') {  //String
            return "string";
        } else {
            size_t len;
            if (isJsonNum(i)) {  //Number
                return "number";
            } else if ((len = isJsonBool(i))) {  //Bool
                return "boolean";
            } else if ((len = isJsonNull(i))) {  //Null
                return "null";
            } else {
                return "";
            }
        }
    }
};

inline std::string_view JSON_Value::typeof(std::string_view str) {
    return parser(str).getType();
}
inline std::unique_ptr<JSON_Value> JSON_Value::ParseAny(std::string_view str) {
    return parser(str)();
}
inline JSON_Object JSON_Value::ParseObject(std::string_view str) {
    checkType(str, "object");
    return std::move(*(JSON_Object*)(ParseAny(str).get()));
}
inline JSON_Number JSON_Value::ParseNumber(std::string_view str) {
    checkType(str, "number");
    return std::move(*(JSON_Number*)(ParseAny(str).get()));
}
inline JSON_String JSON_Value::ParseString(std::string_view str) {
    checkType(str, "string");
    return std::move(*(JSON_String*)(ParseAny(str).get()));
}
inline JSON_Array JSON_Value::ParseArray(std::string_view str) {
    checkType(str, "array");
    return std::move(*(JSON_Array*)(ParseAny(str).get()));
}
inline JSON_Bool JSON_Value::ParseBool(std::string_view str) {
    checkType(str, "bool");
    return std::move(*(JSON_Bool*)(ParseAny(str).get()));
}
inline JSON_Null JSON_Value::ParseNull(std::string_view str) {
    checkType(str, "null");
    return std::move(*(JSON_Null*)(ParseAny(str).get()));
}

inline std::unique_ptr<JSON_Value> to_json(JSON_Number v) { return std::make_unique<JSON_Number>(std::move(v)); }
inline std::unique_ptr<JSON_Value> to_json(JSON_String v) { return std::make_unique<JSON_String>(std::move(v)); }
inline std::unique_ptr<JSON_Value> to_json(JSON_Array v) { return std::make_unique<JSON_Array>(std::move(v)); }
inline std::unique_ptr<JSON_Value> to_json(JSON_Object v) { return std::make_unique<JSON_Object>(std::move(v)); }
inline std::unique_ptr<JSON_Value> to_json(JSON_Bool v) { return std::make_unique<JSON_Bool>(std::move(v)); }
inline std::unique_ptr<JSON_Value> to_json(JSON_Null v) { return std::make_unique<JSON_Null>(std::move(v)); }
