#pragma once
#include <iostream>
#include <cstring>

struct Vec2 {
	int x, y;
	Vec2() = default;

	constexpr Vec2(int x_value, int y_value) : x(x_value), y(y_value) { }

	constexpr Vec2& operator+=(const Vec2& second) {
		this->x += second.x;
		this->y += second.y;
		return *this;
	}

	constexpr Vec2& operator-=(const Vec2& second) {
		this->x -= second.x;
		this->y -= second.y;
		return *this;
	}

	constexpr Vec2 operator-() const {
	    return Vec2(-x, -y);
	}

	friend Vec2 operator+(Vec2 first, const Vec2& second) {
		first += second;
		return first;
	}

	friend Vec2 operator-(Vec2 first, const Vec2& second) {
		first -= second;
		return first;
	}

	friend bool operator==(const Vec2& first, const Vec2& second) {
	    return first.x == second.x && first.y == second.y;
	}

	friend std::ostream& operator<<(std::ostream& o, const Vec2& v) {
	    return o << '(' << v.x << ", " << v.y << ')';
	}

};

template <typename T>
class table_view {
public:
	const size_t w, h, n;
	T* data;
public:
	table_view(T* data, size_t w, size_t h) : w(w), h(h), n(w * h), data(data) { }
	table_view(const table_view&) = default;
	table_view(table_view&&) = default;
	table_view& operator=(table_view&& other) {
	    if (n != other.n) {
            throw std::out_of_range("Table assignment size mismatch");
	    }
	    std::swap(data, other.data);
	    return *this;
	}
	table_view& operator=(const table_view& other) {
	    if (n != other.n) {
            throw std::out_of_range("Table assignment size mismatch");
	    }
        std::memcpy(data, other.data, n * sizeof(T));
        return *this;
	}
	inline T* begin() { return data; }
	inline T* end() { return data + n; }
	inline const T* begin() const { return data; }
	inline const T* end() const { return data + n; }
	inline T& operator()(int x, int y) {
		return data[y * w + x];
	}
	inline const T& operator()(int x, int y) const {
		return data[x + y * w];
	}
	inline T& operator[](const Vec2& vec) {
		return (*this)(vec.x, vec.y);
	}
	inline const T& operator[](const Vec2& vec) const {
		return (*this)(vec.x, vec.y);
		return (*this)(vec.x, vec.y);
	}
	inline T& operator[](size_t i) {
		return data[i];
	}
	inline const T& operator[](size_t i) const {
		return data[i];
	}
	inline size_t xOf(size_t index) const {
		return index % w;
	}
	inline size_t yOf(size_t index) const {
		return index / w;
	}
	inline bool isValid(int x, int y) const {
	    return x >= 0 && y >= 0 && x + y * w < n;
	}
	inline bool isValid(const Vec2& vec) const {
	    return isValid(vec.x, vec.y);
	}
	inline int wrapX(int x) {
        x %= w;
	    if (x < 0) { x += w; }
	    return x;
	}
	inline int wrapY(int y) {
	    y %= h;
	    if (y < 0) { y += h; }
	    return y;
	}
	inline Vec2 wrap(const Vec2& vec) {
        return Vec2(wrapX(vec.x), wrapY(vec.y));
	}
};

template <typename T>
class table : public table_view<T> {
public:
	table(size_t w, size_t h) : table_view<T>(new T[w * h], w, h) { }
	~table() { delete[] table_view<T>::data; }
};
