#pragma once
#include <mutex>
#include <map>
#include <functional>
#include "NetworkedObjects.h"
#include "Broadcaster.h"

class GameLogic {
    std::function<void(std::string_view)> notifyAll;
public:
    GameLogic(WebsocketBroadcaster&);
public:  //Objects
    NetworkedGameField nGameField;
    NetworkedBetList nBetList;
    NetworkedPlayerList nPlayerList;
    NetworkedTimer nTimer;
    NetworkedMoveCounter nMoveCounter;
    NetworkedState nGameState;

    std::mutex all_mutex;

private:  //Root events
    void onTimerTrigger();
public:   //Root events
    Player::ID onPlayerConnected(std::string name, WebsocketConnection&);
    bool onClientPacket(Player::ID, std::string&&);
    void onPlayerDisconnected(Player::ID);

private:
    using PacketHandler = std::function<bool(Player::ID, const JSON_Array&)>;
    std::map<std::string_view, PacketHandler> clientPacketHandlers;
private:  //Secondary events
    void onStartRound();
    void onShowWin();
    void onTurnStart();
    void onShowFail();
    void onTurnReset();
};
