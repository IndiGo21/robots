#include "RobotsServer.h"
#include "libs/SockStream.h"
#include "libs/http.h"
#include "libs/sha1.hpp"
#include "libs/base64.h"

constexpr size_t MAX_NAME_LENGTH = 16;

//Important, broadcaster should be initialized before game
RobotsServer::RobotsServer() : bcast(), game(bcast) { }

void RobotsServer::start() {
    //Raw TCP
	SockServer server;
	server.Start(59286); //��������� TCP ������ �� ����� 59286

    while(true) {
        ConnectedClient cl = server.Accept(); //���� �� ������ ����� ����������� (�����) ���� ���, � ����� ���������� � ����������
        std::cout << "Client connected" << std::endl;

        std::thread([this](auto&& cl){ ClientThread(std::move(cl), bcast, game); }, std::move(cl)).detach(); //���� ������������ ������ � ����, ���� ��������� ���. //� ����� ������
    }
}



RobotsServer::ClientThread::ClientThread(ConnectedClient&& cl, WebsocketBroadcaster& bc, GameLogic& game) try : ClientThread(bc, game) { //����������� ��� ClientThread
    std::string key;
    {  //HTTP request
        std::string s = http::recv_head(cl); //�������� http ���������

        http::http_view hv(s); //������� ������ ������ http_view

        if (!hv.has("Connection", "Upgrade")) { return; }  //���� �� ���� Connection � Upgrade � ���
        if (!hv.has("Upgrade", "websocket")) { return; } // ����������

        key = hv.getAll("Sec-WebSocket-Key"); //��,��� ���� ���

        if (key.empty()) { return; } // � ���� ��� �����, �� �������� �� �������
    }

    {  //HTTP response
        const std::string keyResponse = to_base64(SHA1(std::string(key) + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").final()); //������� ����, ���������� � ���� �������, ������� ������(�������� ���������)

        http::packet response("HTTP/1.1 101 Switching Protocols"); //������� �����
        response.push("Connection", "Upgrade");
        response.push("Upgrade", "websocket");
        response.push("Sec-WebSocket-Accept", keyResponse);

        cl.Send(response.str()); ///���������� �����
    }

    {

        sock_id = bcast.addConnection(WebsocketConnection(std::move(cl), true)); //������� �����������
        WSPacket p = bcast.getConnection(sock_id).Receive(); //������� ����� �� �����������
        if (p.opcode != WSPacket::text) { return; } //���� ����� �� �� ������- ������� �� �������
        if (p.data.empty()) { return; } //���� ������ ������ ����� - ������� �� �������
        if (p.data.size() > MAX_NAME_LENGTH) { return; } //���� ��� ������� ������������� - ������� �� �������

        if (std::any_of(p.data.begin(), p.data.end(), [](auto c) {
            return !(isalnum(c) || c == ' ' || c == '_' || c == '-'); //���� ����� �� �� ���������� �������� -������� �� �������
        })) { return; }
        player_id = game.onPlayerConnected(p.data, bcast.getConnection(sock_id)); //���� ��� �������� ��������, ��  ������������ ������ � ����� � ��� �������
    }

    //Main loop
    while(true) {
        WSPacket p = bcast.getConnection(sock_id).Receive(); //������� ����� �� �����������
        std::cout << "Received " << p.data.size() << " bytes from " << game.nPlayerList.get(player_id).name << std::endl; //������� � �������, ������� ���� �� ������������ ��������
        if (p.opcode == p.connection_close) { //���� ������������ ���������
            std::cout << "Received connection_close from " << game.nPlayerList.get(player_id).name << std::endl; //��������, ��� ������
            break;
        }
        if (!game.onClientPacket(player_id, std::move(p.data))) { //������� ���������� ����, ���� ����� �������� ���-��, ��� �� ������ ���������.
            std::cout << "Kicked " << game.nPlayerList.get(player_id).name << " for malformed packet" << std::endl; //� ���������� ������
            break;  //Kick if client sent a bad packet
        }
    }

} catch (const SockError& e) { //���� �� ��������� ���, ��� ����, � ������� ������,
    std::ostringstream msg; //������� ��������� � �����
    msg << "thread#"; //�����
    msg << std::this_thread::get_id();
    msg << ": " << e.what() << std::endl; //������� ��� �� ������
    std::cout << msg.str(); //������� ����  �������
}

RobotsServer::ClientThread::~ClientThread() {
    bcast.removeConnection(sock_id); //����������� �����������
    if (player_id != Player::INVALID_ID) { //������ �� ������, ���� ��� �� ��� ������� ��������
        game.onPlayerDisconnected(player_id); //��������� ������
    }
}
