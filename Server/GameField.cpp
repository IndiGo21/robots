#include "GameField.h"
#include <array>
#include <ctime>
#include <random>
#include "BoardGenerator.h"

bool GameField::hasRobot(const Vec2& pos) const {
    for (const auto& r : robots) {
        if (r.second == pos) { return true; }
    }
    return false;
}

bool GameField::moveRobot(const RobotColor& color, Vec2 direction) {
    static const auto vec2walls = [](const Vec2& v) -> int {
        if (v.x == 1) {
            return BoardCell::Right;
        } else if (v.x == -1) {
            return BoardCell::Left;
        } else if (v.y == -1) {
            return BoardCell::Top;
        } else {
            return BoardCell::Bottom;
        }
    };
    move_history.emplace(color,robots[color]);
    const int f = vec2walls(direction);
    const int r = vec2walls(-direction);
    size_t i;
    for (i = 0; i < 1000 && !(board[robots[color]].borders & f) &&
           !(board[board.wrap(robots[color] + direction)].borders & r) &&
           !hasRobot(board.wrap(robots[color] + direction)); i++
    ) {
        robots[color] = board.wrap(robots[color] + direction);
    }
    if (i == 1000) { undo(); }
    if (i == 0) {  //Robot didn't move
        undo();
        return false;
    } else {
        return true;
    }
}

bool GameField::onTarget(const RobotColor& color) const {
    return color == target.color && robots.at(color) == target.pos;
}

GameField::RobotColor GameField::undo(){
    RobotColor c = move_history.top().first;
    robots[c] = move_history.top().second;
    move_history.pop();
    return c;
}
void GameField::clearhistory(){
    move_history = std::stack<std::pair<RobotColor,Robot>>();
};
void GameField::moveTarget() {
    static constexpr std::array targs = {
        GameField::Target{Vec2(3-1, 2-1), colorStrToEnum("red")},
        GameField::Target{Vec2(14-1, 2-1), colorStrToEnum("green")},
        GameField::Target{Vec2(2-1, 4-1), colorStrToEnum("green")},
        GameField::Target{Vec2(10-1, 4-1), colorStrToEnum("red")},
        GameField::Target{Vec2(7-1, 5-1), colorStrToEnum("yellow")},
        GameField::Target{Vec2(6-1, 7-1), colorStrToEnum("blue")},
        GameField::Target{Vec2(11-1, 7-1), colorStrToEnum("blue")},
        GameField::Target{Vec2(15-1, 7-1), colorStrToEnum("yellow")},
        GameField::Target{Vec2(4-1, 8-1), colorStrToEnum("silver")},
        GameField::Target{Vec2(6-1, 9-1), colorStrToEnum("red")},
        GameField::Target{Vec2(15-1, 10-1), colorStrToEnum("green")},
        GameField::Target{Vec2(3-1, 11-1), colorStrToEnum("blue")},
        GameField::Target{Vec2(12-1, 11-1), colorStrToEnum("red")},
        GameField::Target{Vec2(10-1, 13-1), colorStrToEnum("blue")},
        GameField::Target{Vec2(5-1, 14-1), colorStrToEnum("green")},
        GameField::Target{Vec2(7-1, 15-1), colorStrToEnum("yellow")},
        GameField::Target{Vec2(14-1, 15-1), colorStrToEnum("yellow")},
    };
    std::uniform_int_distribution<> distr(0, 16);
    //Move target minesweeper style
    for(int i = 0; i < 2000; i++) {
        target = targs[distr(rng)];
        bool collision = false;
        for(const auto& r : robots) {
            if (r.second == target.pos) {
                collision = true;
                break;
            }
        }
        if (collision) { continue; }
        return;
    }
    throw std::runtime_error("Failed to move target");
}


GameField::GameField() : rng(time(0)) {
    const auto placeRobot = [this](auto c) {
        std::uniform_int_distribution<> distr_x(0, board.w - 1);
        std::uniform_int_distribution<> distr_y(0, board.h - 1);
        for(int i = 0; i < 2000; i++) {
            Vec2 pos = { distr_x(rng), distr_y(rng) };
            if ((unsigned) pos.x == board.w / 2 || (unsigned) pos.x == board.w / 2 - 1) { continue; }
            if ((unsigned) pos.y == board.h / 2 || (unsigned) pos.y == board.h / 2 - 1) { continue; }
            bool collision = false;
            for(const auto& r : robots) {
                if (r.second == pos) {
                    collision = true;
                    break;
                }
            }
            if (collision) { continue; }
            this->robots[c] = pos;
            return;
        }
        throw std::runtime_error("Failed to place robot");
    };
    placeRobot(GameField::Red);
    placeRobot(GameField::Green);
    placeRobot(GameField::Blue);
    placeRobot(GameField::Yellow);
    placeRobot(GameField::Silver);

    this->moveTarget();

    board = generateBoard(rng);

    //Borders
    for (size_t i = 0; i < this->board.w; i++) {
        this->board(i, 0).borders |= GameField::BoardCell::Top;
        this->board(i, this->board.h - 1).borders |= GameField::BoardCell::Bottom;
    }
    for (size_t j = 0; j < this->board.h; j++) {
        this->board(0, j).borders |= GameField::BoardCell::Left;
        this->board(this->board.w - 1, j).borders |= GameField::BoardCell::Right;
    }
}
