#pragma once
#include <string>
#include "json_game.h"
#include "Player.h"

namespace updPacket {
    namespace obj {
        inline std::string newBoard(const table<GameField::BoardCell>& t) {
            return JSON_Array::forge("board", t).str();
        }
        inline std::string robotMoved(GameField::RobotColor c, const GameField::Robot& r) {
            return JSON_Array::forge("robot", GameField::colorNames[c], r).str();
        }
        inline std::string allRobotsMoved(const std::map<GameField::RobotColor, GameField::Robot>& robots) {
            return JSON_Array::forge("robots", robots).str();
        }
        inline std::string newTarget(const GameField::Target& t) {
            return JSON_Array::forge("target", t).str();
        }

        inline std::string newPlayer(Player::ID id, const Player& p) {
            return JSON_Array::forge("newplayer", id, p).str();
        }
        inline std::string playerChanged(Player::ID id, const Player& p) {
            return JSON_Array::forge("playerchanged", id, p).str();
        }
        inline std::string delPlayer(Player::ID id) {
            return JSON_Array::forge("delplayer", id).str();
        }

        inline std::string timerFull(double delay, bool isRunning, double timeLeft) {
            return JSON_Array::forge("timerfull", delay, isRunning, timeLeft).str();
        }
        inline std::string timerStarted() {
            return JSON_Array::forge("timerstarted").str();
        }
        inline std::string timerStopped() {
            return JSON_Array::forge("timerstopped").str();
        }

        inline std::string moveCounterChanged(int newVal) {
            return JSON_Array::forge("movecounter", newVal).str();
        }

        inline std::string stateChanged(int newVal) {
            return JSON_Array::forge("state", newVal).str();
        }
    }


    namespace event {
        inline std::string onTimerTrigger() {
            return JSON_Array::forge("timertrigger").str();
        }
        inline std::string onPlayerConnected(Player::ID id) {
            return JSON_Array::forge("playerconnected", id).str();
        }
        inline std::string onPlayerDisconnected(Player::ID id) {
            return JSON_Array::forge("playerdisconnected", id).str();
        }

        inline std::string onTurnStart(Player::ID id) {
            return JSON_Array::forge("turnstart", id).str();
        }
        inline std::string onShowWin(Player::ID id) {
            return JSON_Array::forge("win", id).str();
        }
        inline std::string onShowFail(Player::ID id) {
            return JSON_Array::forge("fail", id).str();
        }
        inline std::string onStartRound() {
            return JSON_Array::forge("startround").str();
        }
        inline std::string onTurnReset() {
            return JSON_Array::forge("turnreset").str();
        }
        inline std::string onBet(Player::ID id, size_t bet) {
            return JSON_Array::forge("newbet", id , bet).str();
        }
    }


    namespace special {
        inline std::string ownId(Player::ID id) {
            return JSON_Array::forge("ownid", id).str();
        }
        inline std::string onInitFinish() {
            return JSON_Array::forge("initdone").str();
        }
    }
}
