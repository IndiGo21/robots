#pragma once
#include <thread>
#include <atomic>
#include <functional>
#include <chrono>
#include "libs/Timer.h"
#include "libs/Stopwatch.h"
#include <iostream>

class NotificationTimer {
public:
    using Callback = std::function<void()>;
    NotificationTimer(Callback cb) : cb(cb) { }
    NotificationTimer(Callback cb, std::chrono::seconds waitTime) : cb(cb), waitTime(std::move(waitTime)) { }
private:
    std::atomic_int timerDisabled = 0;
    bool isTimerOn = false;
    Stopwatch s;
    Callback cb;
public:
	void start() {
	    if (isTimerOn) { stop(); }
	    isTimerOn = true;
	    s.tick();
	    std::thread([&]() {
            std::cout << "Timer started\n";
            Timer timer;
            timer.Set(waitTime);
            timer.Wait();
            if (timerDisabled == 0) {
                isTimerOn = false;
                cb();
            } else {
                timerDisabled--;
            }
        }).detach();
	}
	void stop() {
	    std::cout << "Timer stopped\n";
	    if (!isTimerOn) { return; }
	    timerDisabled++;
	    isTimerOn = false;
	}
	///In seconds, -1 if stopped
	double timeLeft() const {
	    double d = (std::chrono::duration<double, std::milli>(waitTime).count() - s.ping()) / 1000.0;
	    return isTimerOn ? d : -1;
	}
	const std::chrono::seconds waitTime = 60s;
};
