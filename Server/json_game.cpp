#include "json_game.h"

std::unique_ptr<JSON_Value> to_json(const Vec2& vec2) { //������� ����� ������, ���������� �����
    auto ptr = std::make_unique<JSON_Object>();
	JSON_Object& json = *ptr;
    json.pushNumber("x", vec2.x);
    json.pushNumber("y", vec2.y);
    return ptr;
}
std::unique_ptr<JSON_Value> to_json(const GameField::BoardCell& cell) { //������� ����� ������, ���������� ������� ������
    auto ptr = std::make_unique<JSON_Object>();
	JSON_Object& json = *ptr;
    json.pushBool("left", (bool) (cell.borders & cell.Left));
    json.pushBool("right", (bool) (cell.borders & cell.Right));
    json.pushBool("top", (bool) (cell.borders & cell.Top));
    json.pushBool("bottom", (bool) (cell.borders & cell.Bottom));
    return ptr;
}

std::unique_ptr<JSON_Value> to_json(const Player& p) { //������� ����� ������, ���������� ������
    auto ptr = std::make_unique<JSON_Object>();
	JSON_Object& json = *ptr;
    json.pushString("name", p.name);
    json.pushNumber("score", p.score);
    return ptr;
}
std::unique_ptr<JSON_Value> to_json(const GameField& game) { //������� ����� ������, ���������� ���� ����
    auto ptr = std::make_unique<JSON_Object>();
	JSON_Object& json = *ptr;
    json.pushObject("board", game.board);
    json.pushObject("robots", game.robots);
    json.pushObject("target", game.target);
    return ptr;
}
std::unique_ptr<JSON_Value> to_json(const decltype(GameField::robots)& robots) { //������� ����� ������, ���������� ������
    auto ptr = std::make_unique<JSON_Object>();
    JSON_Object& json = *ptr;
    for (const auto& e : robots) {
        json.pushObject(GameField::colorNames[e.first], e.second);
    }
    return ptr;
}
std::unique_ptr<JSON_Value> to_json(const GameField::Target& target) { //������� ����� ������, ���������� �����-����
    auto ptr = std::make_unique<JSON_Object>();
	JSON_Object& json = *ptr;
	json.pushObject("pos", target.pos);
	json.pushString("color", GameField::colorNames[target.color]);
	return ptr;
}
std::unique_ptr<JSON_Value> to_json(const std::map<Player::ID, Player>& v) { //������� ����� ������, ���������� �������
    auto p = std::make_unique<JSON_Object>();
    for (const auto& e : v) {
        p->pushObject(std::to_string(e.first), to_json(e.second));
    }
    return p;
}
