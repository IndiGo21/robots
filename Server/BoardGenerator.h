#pragma once
#include <random>
#include "libs/Table.h"
#include "GameField.h"

table<GameField::BoardCell> generateBoard(std::mt19937& gen);
