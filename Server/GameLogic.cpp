#include "GameLogic.h"
#include "libs/Websocket.h"

constexpr bool freeMove = false;  //For debugging //���������� ����������� ��������

GameLogic::GameLogic(WebsocketBroadcaster& b) //�����������
    : notifyAll( [&b](std::string_view msg) { b.broadcast(msg); } )
    , nGameField(notifyAll)
    , nBetList(notifyAll)
    , nPlayerList(notifyAll)
    , nTimer(notifyAll, [this](){onTimerTrigger();})
    , nMoveCounter(notifyAll)
    , nGameState(notifyAll)
{
    clientPacketHandlers["move"] = [this](Player::ID id, const JSON_Array& a) {
        if (!freeMove) { //���� �� ����� �����������
            if (nGameState != NetworkedState::StateShow) { return true; } //����� ������ ����?
            if (nBetList.getFirst().second != id) { return true; } //��� �� �������, ��� ���, �������?
        }
        if (a.typeof(1) != "string") { return false; } // ���� ����������
        if (a.typeof(2) != "number") { return false; } // ��������� �� �����
        if (a.typeof(3) != "number") { return false; } // �� ��������

        Vec2 dir = Vec2(a.getNumber(2), a.getNumber(3)); //�������� ��  ����� �� ������ � ���� ��� ���������
        if (!(
            (dir.x == 0 && (dir.y == -1 || dir.y == 1)) ||
            (dir.y == 0 && (dir.x == -1 || dir.x == 1))
        )) { return false; }
        GameField::RobotColor color = GameField::colorStrToEnum(a.getString(1)); //����������� �������� �� ������� ����� � �������� �� ����������
        if (color == -1) { return false; } //���� �����������, �� ��������

        bool didMove = nGameField.moveRobot(color, dir); //���� ������������ ������, ��� �� 0 ������
        if (!didMove) { return true; }
        nMoveCounter.inc(); //��������� ����� �����

        if (nGameField.onTarget(color)) { //���� ����� �� ������ �����-����, ��
            nTimer.stop(); //������ ���������������
            onShowWin(); //���������� �����������
        } else if (nMoveCounter >= nBetList.getBet(id)) { //���� ����� ����� ������, ��� ������
            onTurnReset();//�������� ����� �����
        }
        return true;
    };

    clientPacketHandlers["bet"] = [this](Player::ID id, const JSON_Array& a) {
        if (nGameState == NetworkedState::StateShow) { return true; }
        if (a.typeof(1) != "number")  { return false; }
        size_t newbet = a.getNumber(1);
        if(nBetList.push(id, newbet)) {  //If bet was successful
            if (nGameState == NetworkedState::StateIdle) {
                nGameState.set(NetworkedState::StateBets);
                nTimer.start();
            }
            notifyAll(updPacket::event::onBet(id,newbet));
        }
        return true;
    };
    clientPacketHandlers["undo"] = [this](Player::ID id, const JSON_Array& a) {
        if (nMoveCounter.get() > 0) {
            nGameField.undo();
            nMoveCounter.dec();
        }
        return true;
    };
    clientPacketHandlers["reset"] = [this](Player::ID id, const JSON_Array& a) {
        onTurnReset();
        return true;
    };
    onStartRound();
}


void GameLogic::onTimerTrigger() {
    std::lock_guard lock(all_mutex);

    notifyAll(updPacket::event::onTimerTrigger());
    if (nGameState == NetworkedState::StateBets) {
        nGameState.set(NetworkedState::StateShow);
        onTurnStart();
    } else if (nGameState == NetworkedState::StateShow) {
        onShowFail();
    }
}

Player::ID GameLogic::onPlayerConnected(std::string name, WebsocketConnection& sock) {
    std::lock_guard lock(all_mutex);

    //create the new player
    Player::ID id = nPlayerList.push(Player(std::move(name)));


    { //send game info
        sock.Send( updPacket::special::ownId(id) );
        sock.Send( updPacket::obj::newBoard(nGameField.getBoard()) );
        sock.Send( updPacket::obj::allRobotsMoved(nGameField.getRobots()) );
        sock.Send( updPacket::obj::newTarget(nGameField.getTarget()) );
        sock.Send( updPacket::obj::timerFull(nTimer.waitTime.count(), nTimer.timeLeft() != -1, nTimer.timeLeft()) );
        sock.Send( updPacket::obj::moveCounterChanged(nMoveCounter.get()) );
        for (const auto& p : nPlayerList) {
            sock.Send(updPacket::obj::newPlayer(p.first, p.second));
        }
    }

    notifyAll(updPacket::event::onPlayerConnected(id));

    sock.Send( updPacket::special::onInitFinish() );
    return id;
}

bool GameLogic::onClientPacket(Player::ID id, std::string&& msg) {
    std::lock_guard lock(all_mutex);

    try {
        if (JSON_Value::typeof(msg) != "array") { return false; }
        JSON_Array a = JSON_Value::ParseArray(msg);
        if (a.typeof(0) != "string") { return false; }
        const std::string& event = a.getString(0);
        const auto& pHandler = clientPacketHandlers.find(event);
        if (pHandler != clientPacketHandlers.end()) {
            return pHandler->second(id, a);
        } else {
            return false;
        }
    } catch (const JSON_Value::exception& e) {
        return false;
    }
}


void GameLogic::onPlayerDisconnected(Player::ID id) {
    std::lock_guard lock(all_mutex);
    notifyAll(updPacket::event::onPlayerDisconnected(id));
    nPlayerList.pop(id);
    nBetList.del(id);
}


void GameLogic::onStartRound() {
    nGameState.set(NetworkedState::StateIdle);
    nGameField.saveRobots();
    nGameField.moveTarget();
    nGameField.clearhistory();
    nMoveCounter.reset();
    notifyAll(updPacket::event::onStartRound());
}

void GameLogic::onShowWin() {
    Player::ID id = nBetList.empty() ? Player::INVALID_ID : nBetList.getFirst().second;
    notifyAll(updPacket::event::onShowWin(id));

    if (!nBetList.empty()) {
        const auto& entry = nBetList.getFirst();
        const Player::ID& id = entry.second;
        nPlayerList.incScore(id);
    }
    nBetList.clear();

    onStartRound();
}

//expects at least one bet
void GameLogic::onTurnStart() {
    notifyAll(updPacket::event::onTurnStart(nBetList.getFirst().second));
    nMoveCounter.reset();
    nTimer.start();
}

void GameLogic::onShowFail() {
    Player::ID id = nBetList.empty() ? Player::INVALID_ID : nBetList.getFirst().second;
    notifyAll(updPacket::event::onShowFail(id));

    nGameField.loadRobots();

    if (!nBetList.empty()) {
        const auto& entry = nBetList.getFirst();
        const Player::ID& id = entry.second;
        nPlayerList.decScore(id);
    }

    if (nBetList.delFirst()) {  //Next player's turn
        onTurnStart();
    } else {  //Next round
        onStartRound();
    }
}

void GameLogic::onTurnReset() {
    notifyAll(updPacket::event::onTurnReset());
    nMoveCounter.reset();
    nGameField.loadRobots();
    nGameField.clearhistory();
}

