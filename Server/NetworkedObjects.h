#pragma once
#include <iostream>
#include <functional>
#include <string_view>
#include <map>
#include <stdexcept>

#include "Packets.h"
#include "GameField.h"
#include "BetList.h"
#include "NotificationTimer.h"
#include "Player.h"


class NetworkedGameField : private GameField {
    std::function<void(std::string_view)> notifyAll;
public:
    NetworkedGameField(std::function<void(std::string_view)> notifier) : notifyAll(notifier) { }
    ///Warning: doesn't account for modifications, use with caution
    GameField& getRaw() {
        return *this;
    }

    const table<BoardCell>& getBoard() const { return board; }
//    void setBoard(table<BoardCell> v) {
//        board = std::move(v);
//        notifyAll(updPacket::obj::newBoard(board));
//    }

    const std::map<RobotColor, Robot>& getRobots() const { return robots; }
    bool moveRobot(RobotColor color, const Vec2& direction) {
        bool ret = GameField::moveRobot(color, direction);
        if (ret) {
            notifyAll(updPacket::obj::robotMoved(color, GameField::robots[color]));
        }
        return ret;
	}
	RobotColor undo(){
	    auto c = GameField::undo();
	    notifyAll(updPacket::obj::robotMoved(c, GameField::robots[c]));
	    return c;
	}
	using GameField::clearhistory;
	void saveRobots() {
	    GameField::saveRobots();
	}
	void loadRobots() {
	    GameField::loadRobots();
	    notifyAll(updPacket::obj::allRobotsMoved(robots));
	}

	const Target& getTarget() const { return target; }
    void setTarget(Target v) {
        target = std::move(v);
        notifyAll(updPacket::obj::newTarget(target));
    }
    void moveTarget() {
        GameField::moveTarget();
        notifyAll(updPacket::obj::newTarget(target));
    }
    using GameField::onTarget;

    std::string serialize() const { return to_json((GameField) *this)->str(); }
};

//Nothing gets sent here
class NetworkedBetList : public BetList {
public:
    NetworkedBetList() = default;
    NetworkedBetList(std::function<void(std::string_view)>) { }
};

class NetworkedPlayerList {
    std::map<Player::ID, Player> players;

    Player::ID idGen = 0;
    Player::ID getNewId() {
        return idGen++;
    }
    Player& getw(Player::ID id) {
        auto it = players.find(id);
        if (it != players.end()) {
            return it->second;
        } else {
            throw std::out_of_range("No player found with the given id");
        }
    }
    std::function<void(std::string_view)> notifyAll;
public:
    NetworkedPlayerList(std::function<void(std::string_view)> notifier) : notifyAll(notifier) { }
    Player::ID push(Player&& p) {
        const auto id = getNewId();
        players.emplace_hint(players.end(), id, p);
        notifyAll(updPacket::obj::newPlayer(id, p));
        return id;
    }
    bool pop(Player::ID id) {
        bool erased = players.erase(id);
        if (erased) {
            notifyAll(updPacket::obj::delPlayer(id));
        }
        return erased;
    }
    const Player& get(Player::ID id) const {
        auto it = players.find(id);
        if (it != players.end()) {
            return it->second;
        } else {
            throw std::out_of_range("No player found with given id");
        }
    }
    const Player& operator[](Player::ID id) const {
        return get(id);
    }

    void decScore(Player::ID id) {
        Player& p = getw(id);
	    if (p.score > 0) {
            p.score--;
            notifyAll(updPacket::obj::playerChanged(id, p));
        }
	}
	void incScore(Player::ID id) {
	    Player& p = getw(id);
	    p.score++;
	    notifyAll(updPacket::obj::playerChanged(id, p));
    }

    auto begin() const { return players.cbegin(); }
    auto end() const { return players.cend(); }
};

class NetworkedTimer : private NotificationTimer {
    std::function<void(std::string_view)> notifyAll;
public:
    NetworkedTimer(std::function<void(std::string_view)> notifier, Callback onTrigger) : NotificationTimer(onTrigger), notifyAll(notifier) { }
    void start() {
        NotificationTimer::start();
        notifyAll(updPacket::obj::timerStarted());
    }
    void stop() {
	    NotificationTimer::stop();
	    notifyAll(updPacket::obj::timerStopped());
	}
	using NotificationTimer::timeLeft;
	using NotificationTimer::waitTime;
};

class NetworkedMoveCounter {
    size_t timesMoved = 0;
    std::function<void(std::string_view)> notifyAll;
public:
    NetworkedMoveCounter(std::function<void(std::string_view)> notifier) : notifyAll(notifier) { }
    void inc() {
        timesMoved++;
        notifyAll(updPacket::obj::moveCounterChanged(timesMoved));
    }
    void dec() {
        if (timesMoved > 0) {
            timesMoved--;
            notifyAll(updPacket::obj::moveCounterChanged(timesMoved));
        }
    }
    void reset() {
        timesMoved = 0;
        notifyAll(updPacket::obj::moveCounterChanged(timesMoved));
    }

    const size_t& get() const {
        return timesMoved;
    }
    operator size_t() const {
        return timesMoved;
    }
};

class NetworkedState {
	int state = StateIdle;
    std::function<void(std::string_view)> notifyAll;
public:
    NetworkedState(std::function<void(std::string_view)> notifier) : notifyAll(notifier) { }
    enum { StateIdle = 0, StateBets = 1, StateShow = 2 };
    void set(int newState) {
        state = newState;
        notifyAll(updPacket::obj::stateChanged(state));
    }
    const int& get() const {
        return state;
    }
    operator int() const {
        return state;
    }
};
