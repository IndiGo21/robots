#pragma once
#include <map>
#include "Player.h"

class BetList {
    std::multimap<size_t, Player::ID> data;
public:
    bool push(Player::ID id, size_t bet) {
        for (auto it = data.begin(); it != data.end(); ++it) {
            if (it->second == id) {
                if (bet < it->first) {
                    data.erase(it);
                    data.emplace(bet, id);
                    return true;
                } else {
                    return false;
                }
            }
        }
        data.emplace(bet, id);
        return true;
    }
    size_t getBet(Player::ID id) const {
        for (const auto& e : data) {
            if (e.second == id) { return e.first; }
        }
        return -1;
    }
    bool has(Player::ID id) const {
        for (const auto& e : data) {
            if (e.second == id) { return true; }
        }
        return false;
    }
    bool empty() const { return data.empty(); }
    void clear() { return data.clear(); }
    bool del(Player::ID id) {
        for (auto it = data.begin(); it != data.end(); ++it) {
            if (it->second == id) {
                data.erase(it);
                return true;
            }
        }
        return false;
    }
    std::pair<size_t, Player::ID> getFirst() const {
        return *data.begin();
    }
    bool delFirst() {
        if (!data.empty()) {
            data.erase(data.begin());
        }
        return !data.empty();
    }
};
