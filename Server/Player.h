#pragma once
#include <string>

struct Player {
    Player(std::string name) : name(std::move(name)) { }

	std::string name;
	size_t score = 0;

	using ID = uint32_t;
	static constexpr ID INVALID_ID = -1;
};
